# v0.0.7: Wednesday, April 22 2020
* User crud
* Algolia places implementation on user address
* Passport and Expiry date added
* Worker, Gender and Birthday icons added

# v0.0.6: Tuesday, April 14th 2020
* Autocomplete field for address (algolia) added to destination
* field: firstname validated
* field: lastname validated
* validate field: email validated
* display map with wishlist countries added
* display amount of visited countries/total amount of countries added

# v0.0.5: Thursday, April 9th 2020
* Finish Category CRUD
* User Seeder
* User factory
* User can add visited countries
* Display map with visited countries

# v0.0.4: Wednesday, April 8th 2020
* Change the Theme MVC for Category
* Seeder with description texts
* Validate name field
* Added: academic, religion, and gastronomy categories
* Academic icon added
* Religion icon added
* Discovery icon added

# v0.0.3: Tuesday, April 7th 2020
* Import Rubik font from Google Fonts
* Top navbar background color changed to white
* Sidebar structure added
* Email circled icon added 
* Phone circled icon added 

# v0.0.2: Tuesday, March 10th 2020
* Themes CRUD
* Kirby design layout
* Themes pagination
* Themes search bar

# v0.0.1 : Saturday, March 7th 2020
* Country CRUD