# TODO

## Enquiry Module
* CRUD Enquiry
* Validate fields
* Sanatize fields
* Autocomplete destination
* Relationship Enquiry - Country
* Relationship Enquiry - User
* Relationship Enquiry - Category
* Email User
* Email Agencies
* Enquiry by Theme/Purpose
* check if place exists, if it does counter++
* check if country existes, if it does counter++
* Check if category existes, if it does counter++
- add https://lokesh-coder.github.io/pretty-checkbox/
## User Module
* CRUD User
* ~~User Seeder~~
* ~~User factory~~
* ~~Add autocomplete field for address (algolia)~~
* ~~validate field: firstname~~
* ~~validate field: lastname~~
* ~~validate field: email~~
* ~~display map with wishlist countries~~
* ~~display amount of visited countries/total amount of countries~~

* validate field: phone(s)
* validate field: birthday
* validate field: profession
* validate field: social networks
* validate field: address
* validate field: languages
* validate field: default language
* validate field: nationalities

* Sanatize fields
* Verify email
* Verify phone
* passport info
* passpoert expiry date reminder
* user friendships
* User roles and abilities
* User profile
* Activities/Hobbies
* Prefered language
* dashboard
* ~~User can add visited countries~~
* User can add wishlist countries
* User can add wishlist places
* User can add visited place
* bucketlist
* ~~display map with visited countries~~


## bucketlist model
* Relationship user-activity ToDo 
* Relationship user-places ToGo

# Module Country
* Add seeder for Country
* Script to load Countries table from https://restcountries.eu
* Add algolia autocomplete to fetch date from algolia and restcountries to fill in the fields
* Add fields to Model Country (wikipedia, restcountries fields, google_id, official tourist office website, media website, add photos)

# Module Activities
* Activities CRUD
* Validate field: name
* Validate field: description
* Validate field: icon
* Relationship with Category

## Module Categories
* ~~Change the Theme MVC for Category~~
* ~~Finish Category CRUD~~
* ~~Seeder~~
* ~~Validate name fields~~ 
* ~~Add academic, religion, gastronomy~~
* ~~add icon academic~~
* ~~add icon religion~~
* Validate icon fields 
* Validate description fields 
* Test Themes module

## Module Agencies
* CRUD agencies
* Field Validation
* Field Sanitation
* Factory
* Seeder
* Test Agency modules
* Agency-category relationship
* Agency-country relationship
* Agency-activity relationship

## Module Place 
* add Model Place
* CRUD Place
* Validate fields (same as algolia places)
* Factory
* Seeder

## Challenge Module
* CRUD challenge
* validate fields (name, activities, places, countries, points, difficulty)


- Validate Agency
- Validate Country
- Validate Enquiry
- Validate Users
- ajouter photos to Agency

## NGO Module

## General
* ~~Sidebar menu (country, agencies, places, enquiries, bucketlist, wishlist, challenges)~~
* ~~Add rubik font~~
* Deploy Infomaniak, kreativmedia
* Improve the menu
* Create landing page
* Cookie consent
* Conditions general de vente
* Fix login template (view layout)
* Fix register template (view layout)
* Finish working on the layout, make it look good
* List pagination and search with vue [tutorial](https://www.youtube.com/watch?v=FJ1MaNtjPDs)
* Create a page for icons
* [switch buttons](https://www.w3schools.com/howto/howto_css_switch.asp)
* Move admin menu links to the bottom of the page
- Add header for each page

## Miscellenious
* https://appfinz.com/blogs/laravel-shift-blueprint/
* https://github.com/laravel-shift/blueprint
- https://github.com/laracasts/PHP-Vars-To-Js-Transformer
- https://stackoverflow.com/questions/32398505/get-current-user-in-javascript-jquery-laravel-5-1
- https://www.amcharts.com/demos/rotating-globe-with-circles/?theme=frozen
- https://laravel-livewire.com
- https://www.w3schools.com/tags/tag_datalist.asp
- avatarmaker.net

- Visited Countries list or Bucketlist countries list must display only the corresponding countries, we should not find the same country in both lists
- toggle sidebar https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_collapse_sidebar


# Countdown
* https://www.w3schools.com/howto/howto_js_countdown.asp
* https://codepen.io/AllThingsSmitty/pen/JJavZN


- Copy keys in config file, not on github
https://formvalidation.io/#features