<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Country extends Model
{
    protected $fillable = [
        'name', 'slug', 'flag',
    ]; 

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function agencies()
    {
        return $this->belongsToMany(Company::class);
    }

    public function enquiries()
    {
        return $this->belongsToMany(Enquiry::class);
    }

}


