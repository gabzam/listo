<?php

namespace App\Models;

use App\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enquiry extends Model
{
    use SoftDeletes;

    protected $fillable = [
        
        'travel_type',

        // Trip 1
        'from_1',
        'from_country_1',
        'from_country_code_1',
        'from_lat_1',
        'from_lng_1',

        'to_1',
        'to_country_1',
        'to_country_code_1',
        'to_lat_1',
        'to_lng_1',

        'departure_date_1',
        'return_date_1',

        // Trip 2
        'from_2',
        'from_country_2',
        'from_country_code_2',
        'from_lat_2',
        'from_lng_2',

        'to_2',
        'to_country_2',
        'to_country_code_2',
        'to_lat_2',
        'to_lng_2',

        'departure_date_2',

        // Trip 3          

        'from_3',
        'from_country_3',
        'from_country_code_3',
        'from_lat_3',
        'from_lng_3',

        'to_3',
        'to_country_3',
        'to_country_code_3',
        'to_lat_3',
        'to_lng_3',

        'departure_date_3',

        // Trip 4

        'from_4',
        'from_country_4',
        'from_country_code_4',
        'from_lat_4',
        'from_lng_4',

        'to_4',
        'to_country_4',
        'to_country_code_4',
        'to_lat_4',
        'to_lng_4',

        'departure_date_4',

        // Info

        'flexibility',
        
        'traveling_with',
        'nb_adults',
        'nb_kids',
        'nb_babies',

        'options',

        'budget_min',
        'budget_max',
        'comments',

        'flight_class',
        'meal_type',
        'seat_type',
        'flight_comments',

        'accommodation_type',
        'accommodation_rating',

        'transportation_type',
        'car_type',
        'transportation_comments',

        'status',

        'contact_mode',

        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    public function agencies()
    {
        return $this->belongsToMany(Company::class);
    }

    public function companiesEmails()
    {
        return $this->agencies()->pluck('email')->unique();
    }
}
