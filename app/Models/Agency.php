<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'name',
        'description',
        'email',
        'phone',
        'website',
        'employees',
        'has_branches',
        'has_insurance',
        'has_newsletter',
        'founded',
        'defunct',
        'country',
        'state',
        'postal_code',
        'location',
        'street',
        'street_number',
        'address_info',
        'facebook',
        'twitter',
        'instagram',
        'linkedin',
        'snapchat',
        'blog',
        'skype',
        'pinterest',
        'youtube',
        'lat',
        'lng',
        'status',
        'travel_type',
        'type',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function hasCategory($cat_id)
    {
        return in_array($cat_id, $this->categories->pluck('id')->toArray());
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    public function enquiries()
    {
        return $this->belongsToMany(Enquiry::class);
    }

    public function hasCountry($country_id)
    {
        return in_array($country_id, $this->countries->pluck('id')->toArray());
    }

    public function isType($value)
    {
        $types = explode(',', $this->type);        
        return in_array($value, $types);
    }
}



