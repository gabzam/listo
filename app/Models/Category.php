<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',
        'icon'
    ];

    public function agencies()
    {
        return $this->belongsToMany(Company::class);
    }

    public function enquiries()
    {
        return $this->belongsToMany(Enquiry::class);
    }
}
