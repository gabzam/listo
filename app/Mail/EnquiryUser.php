<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EnquiryUser extends Mailable
{
    use Queueable, SerializesModels;
    public $enquiry;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\User $user, \App\Models\Enquiry $enquiry)
    {
        $this->user = $user;
        $this->enquiry = $enquiry;        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.enquiry-user')->subject('Listo: Request Sent');
    }
}
