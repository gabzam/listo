<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Country;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search =request()->query('search');
        if ($search) {
            $users = User::where('firstname','LIKE', "%{$search}%")->simplePaginate(10);
        }else{
            $users = User::simplePaginate(10);
        }
        return view('users.index')->with('users', $users);        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {           
        return view('users.create')->with('countries', Country::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        //dd($request->all());
        
        User::create([
            'firstname'     => $request->firstname,
            'lastname'      => $request->lastname,    
            
            'username'      => $request->username,     
            'password'      => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',       
            'email'         => $request->email,
            'mobile'        => $request->mobile,
            'phone'         => $request->phone,
            
            'birthday'      => $request->birthday,
            'gender'        => $request->gender,
            'profession'    => $request->profession,
            
            'whatsapp'      => $request->has('whatsapp'),
            'viber'         => $request->has('viber'),
            'wechat'        => $request->has('wechat'),
            'telegram'      => $request->has('telegram'),
            'call'          => $request->has('call'),
            'sms'           => $request->has('sms'),
            
            'street'                => $request->street,
            'street_number'         => $request->street_number,
            'address_info'          => $request->address_info,
            'postal_code'           => $request->postal_code,
            'location'              => $request->location,
            'state'                 => $request->state,
            'country'               => $request->country,
            'lat'                   => $request->lat,
            'lng'                   => $request->lng,
            
            'facebook'      => $request->facebook,
            'linkedin'      => $request->linkedin,
            'instagram'     => $request->instagram,
            'youtube'       => $request->youtube,
            'tiktok'        => $request->twitter,
            'skype'         => $request->skype,
            'snapchat'      => $request->snapchat,
            'pinterest'     => $request->pinterest,
            
            'passport1'         => $request->passport1,
            'passport2'         => $request->passport2,
            'passport3'         => $request->passport3,
            'passport4'         => $request->passport4,
            'passport1_expiry'  => $request->passport1_expiry,            
            'passport2_expiry'  => $request->passport2_expiry,        
            'passport3_expiry'  => $request->passport3_expiry,
            'passport4_expiry'  => $request->passport4_expiry,
            
        ]);
        // email_verified_at
        // mobile_verified_at
        // phone_verified_at                                     

        session()->flash('success', 'User created successfully. Your password was generated automatically, please check your emails');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit')
                ->with('user', $user)
                ->with('countries', Country::all())
                ->with('roles',Role::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {        
        //dd($request->all());
        $user->update([
            'firstname'     => $request->firstname,
            'lastname'      => $request->lastname,    
            
            'username'      => $request->username,     
                
            'email'         => $request->email,
            'mobile'        => $request->mobile,
            'phone'         => $request->phone,
            
            'birthday'      => $request->birthday,
            'gender'        => $request->gender,
            'profession'    => $request->profession,
            
            'whatsapp'      => $request->has('whatsapp'),
            'viber'         => $request->has('viber'),
            'wechat'        => $request->has('wechat'),
            'telegram'      => $request->has('telegram'),
            'call'          => $request->has('call'),
            'sms'           => $request->has('sms'),
            
            'street'                => $request->street,
            'street_number'         => $request->street_number,
            'address_info'          => $request->address_info,
            'postal_code'           => $request->postal_code,
            'location'              => $request->location,
            'state'                 => $request->state,
            'country'               => $request->country,
            'lat'                   => $request->lat,
            'lng'                   => $request->lng,
            
            'facebook'              => $request->facebook,
            'linkedin'              => $request->linkedin,
            'instagram'             => $request->instagram,
            'youtube'               => $request->youtube,
            'tiktok'                => $request->twitter,
            'skype'                 => $request->skype,
            'snapchat'              => $request->snapchat,
            'pinterest'             => $request->pinterest,
            
            'passport1'             => $request->passport1,
            'passport2'             => $request->passport2,
            'passport3'             => $request->passport3,
            'passport4'             => $request->passport4,
            'passport1_expiry'      => $request->passport1_expiry,            
            'passport2_expiry'      => $request->passport2_expiry,        
            'passport3_expiry'      => $request->passport3_expiry,
            'passport4_expiry'      => $request->passport4_expiry,
        ]);

        if ($request->roles) {
            $user->roles()->sync($request->roles);            
        }

        if ($request->hasFile('picture')) {

            //$picture = request()->file('picture')->getClientOriginalName();
            //request()->file('picture')->storeAs('avatars', $user->id . '/' . $picture);
            $user->update(['picture' => $request->picture->store('avatars') ]);
        }
        
        session()->flash('success','User updated successfully');

        return redirect(route('profile.show', auth()->user()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        session()->flash('success', 'User deleted successfully');
        
        return redirect(route('users.index')); 
        
    }
}


