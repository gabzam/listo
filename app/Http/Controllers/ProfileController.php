<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function show(User $user)
    {            
        return view('profile.show')->with('user', $user);
    }

    public function edit(User $user)
    {
        return view('profile.edit')->with('user', $user);
    }

    public function enquiries(User $user)
    {
        return view('profile.enquiries')->with('user', $user);
    }

    public function visit()
    {
        return view('profile.visit');
    }
    public function learn()
    {
        return view('profile.learn');
    }
    public function experience()
    {
        return view('profile.experience');
    }
}
