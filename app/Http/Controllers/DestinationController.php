<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\User;
use JavaScript;

class DestinationController extends Controller
{
    public function index()
    {
        // JavaScript::put([
        //         'gabo' => 'Gabriel es el mejor'
        // ]);
        
        $visitedCountries = Country::all();

        //dd(visitedCountries);

        return view('destinations.index')
            ->with('countryList', Country::all())
            ->with('user', auth()->user());
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $search = $request->country;
        $user = User::find(auth()->user()->id);

        $country = Country::where('name', 'LIKE', "%{$search}%")->first();

        $countries = $user->visitedCountries;

        if ( $countries->contains('id', $country->id) ) {
            session()->flash('error', 'The country you selected is already in the list');
        } else{
            if ($request->status == 'visited') {
                $user->visitedCountries()->attach($country->id, ['status' => $request->status]);
            } else {
                $user->bucketListCountries()->attach($country->id, ['status' => $request->status]);
            }
            session()->flash('success', 'Country added to the list successfully');
        }
        //dd($countries->toArray());        
        //dd($countries->contains('id', $country->id));
        //pluck('countries')->toArray()

        return redirect()->back();
    }

    public function destroy($id, Request $request)
    {
        $user = $user = User::find(auth()->user()->id);
        if ($request->status == 'bucketlist') {
            $user->bucketListCountries()->detach($id);
        } else {
            $user->visitedCountries()->detach($id);
        }

        session()->flash('success', 'Country removed from list successfully');
        return redirect()->back();
    }
}
