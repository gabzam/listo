<?php

namespace App\Http\Controllers;

use App\Models\Agency;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Requests\Agency\StoreAgencyRequest;
use App\Http\Requests\Agency\UpdateAgencyRequest;
use App\Models\Category;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search =request()->query('search');
        if ($search) {
            $agencies = Agency::where('name','LIKE', "%{$search}%")->simplePaginate(10);
        }else{
            $agencies = Agency::simplePaginate(10);
        }
        
        if (Agency::onlyTrashed()->get()->count() > 0) {
            $trashed_button = true;
        }
        
        return view('agencies.index')
            ->with('agencies', $agencies)
            ->with('title','Agencies')
            ->with('trashed_button', $trashed_button ?? false);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agencies.create')
            ->with('categories', Category::all())
            ->with('countries', Country::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgencyRequest $request)
    {
                //dd($request->all());
                $agency = Agency::create([
                    'name'              => $request->name,
                    'description'       => $request->description,
                    'email'             => $request->email,
                    'phone'             => $request->phone,
                    'website'           => $request->website,            
                    
                    'street'            => $request->street,
                    'street_number'     => $request->street_number,
                    'address_info'      => $request->address_info,
                    'location'          => $request->location,
                    'postal_code'       => $request->postal_code,
                    'state'             => $request->state,
                    'country'           => $request->country,
                    'lat'               => $request->lat,
                    'lng'               => $request->lng,
        
                    'employees'         => $request->employees,
                    'has_branches'      => $request->has_branches   === 'on' ? true : false,
                    'has_insurance'     => $request->has_insurance  === 'on' ? true : false,
                    'has_newsletter'    => $request->has_newsletter === 'on' ? true : false,
                    'founded'           => $request->founded,
                    'defunct'           => $request->defunct,
        
                    'facebook'          => $request->facebook,
                    'twitter'           => $request->twitter,
                    'instagram'         => $request->instagram,
                    'linkedin'          => $request->linkedin,
                    'snapchat'          => $request->snapchat,
                    'blog'              => $request->blog,
                    'skype'             => $request->skype,
                    'pinterest'         => $request->pinterest,
                    'youtube'           => $request->youtube,
        
        
                    'status'            => $request->status,
                    //'agency_type'       => isset($request->agency_type) ? implode(' | ', $request->agency_type) : '',
                ]);

                if ($request->categories) {
                    $agency->categories()->attach($request->categories);
                }
                if ($request->countries) {
                    $agency->countries()->attach($request->countries);
                }
        
                session()->flash('success','Travel Agency added successfully');
        
                return redirect(route('agencies.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function show(Agency $agency)
    {
        return view('agencies.show')->with('agency', $agency);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function edit(Agency $agency)
    {
        return view('agencies.edit')
            ->with('agency', $agency)
            ->with('categories', Category::all())
            ->with('countries', Country::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgencyRequest $request, Agency $agency)
    {
        //dd($request->all());
        $agency->update([
            'name'              => $request->name,
            'description'       => $request->description,
            'email'             => $request->email,
            'phone'             => $request->phone,
            'website'           => $request->website,            
            
            'street'            => $request->street,
            'street_number'     => $request->street_number,
            'address_info'      => $request->address_info,
            'postal_code'       => $request->postal_code,
            'location'          => $request->location,
            'state'             => $request->state,
            'country'           => $request->country,
            'lat'               => $request->lat,
            'lng'               => $request->lng,

            'employees'         => $request->employees,
            'has_branches'      => $request->has_branches   === 'on' ? true : false,
            'has_insurance'     => $request->has_insurance  === 'on' ? true : false,
            'has_newsletter'    => $request->has_newsletter === 'on' ? true : false,
            'founded'           => $request->founded,
            'defunct'           => $request->defunct,

            'facebook'          => $request->facebook,
            'twitter'           => $request->twitter,
            'instagram'         => $request->instagram,
            'linkedin'          => $request->linkedin,
            'snapchat'          => $request->snapchat,
            'blog'              => $request->blog,
            'skype'             => $request->skype,
            'pinterest'         => $request->pinterest,
            'youtube'           => $request->youtube,

            'status'            => $request->status,            
            'type'              => $request->has('type') ? implode(',', $request->type) : '',
        ]);

        if ($request->categories) {
            $agency->categories()->sync($request->categories);
        }
        if ($request->countries) {
            $agency->countries()->sync($request->countries);
        }

        session()->flash('success','Agency updated successfully');

        return redirect(route('agencies.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        $agency = Agency::withTrashed()->where('id',$id)->firstOrFail();

        if ($agency->trashed()) {
            $agency->forceDelete();
        } else {
            $agency->delete();
        }
        
        session()->flash('success', 'Travel Agency successfully deleted');

        return redirect(route('agencies.index'));
    }

     /**
     * Display a listing of trashed agencies.
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {        
        $search =request()->query('search');
        if ($search) {
            $trashed = Agency::onlyTrashed('name','LIKE', "%{$search}%")->get();
        }else{
            $trashed = Agency::onlyTrashed()->get();            
        }
        return view('agencies.trashed')->with('agencies', $trashed);
    }

    public function show_trashed($id)
    {
        $agency = Agency::withTrashed()->find($id);
        return view('agencies.show')->with('agency', $agency);

    }

    public function restore($id)
    {
        $agency = Agency::withTrashed()->find($id);
        $agency->restore();

        session()->flash('success','Agency restored successfully');

        return redirect(route('agencies.index'));
    }
}

