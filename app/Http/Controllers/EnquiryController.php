<?php

namespace App\Http\Controllers;

use App\Models\Enquiry;
use App\Models\Category;
use App\Models\Country;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\Enquiry\CreateEnquiryRequest;
use App\Mail\Enquiry as MailEnquiry;
use App\Mail\EnquiryCompanies;
use App\Mail\EnquiryUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Mail;

class EnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->query('search');
        if ($search) {
            $enquiries = Enquiry::where('destination_location', 'LIKE', "%{$search}%")->simplePaginate(10);
        } else {
            $enquiries = Enquiry::simplePaginate(10);
        }
        return view('enquiries.index')->with('enquiries', $enquiries);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('enquiries.create')->with('categories', Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEnquiryRequest $request)
    {
        //dd($request->all());
        //dd(auth()->user()->id);

        $enquiry = Enquiry::create([

            'travel_type'            => $request->travel_type,

            // Trip 1  

            'from_1'                => $request->from_1,
            'from_country_1'        => $request->from_country_1,
            'from_country_code_1'   => $request->from_country_code_1,
            'from_lat_1'            => $request->from_lat_1,
            'from_lng_1'            => $request->from_lng_1,

            'to_1'                  => $request->to_1,
            'to_country_1'          => $request->to_country_1,
            'to_country_code_1'     => $request->to_country_code_1,
            'to_lat_1'              => $request->to_lat_1,
            'to_lng_1'              => $request->to_lng_1,

            'departure_date_1'      => $request->departure_date_1,
            'return_date_1'         => $request->return_date_1,

            // Trip 2

            'from_2'                => $request->from_2,
            'from_country_2'        => $request->from_country_2,
            'from_country_code_2'   => $request->from_country_code_2,
            'from_lat_2'            => $request->from_lat_2,
            'from_lng_2'            => $request->from_lng_2,

            'to_2'                  => $request->to_2,
            'to_country_2'          => $request->to_country_2,
            'to_country_code_2'     => $request->to_country_code_2,
            'to_lat_2'              => $request->to_lat_2,
            'to_lng_2'              => $request->to_lng_2,

            'departure_date_2'      => $request->departure_date_2,            

            // Trip 3

            'from_3'                => $request->from_3,
            'from_country_3'        => $request->from_country_3,
            'from_country_code_3'   => $request->from_country_code_3,
            'from_lat_3'            => $request->from_lat_3,
            'from_lng_3'            => $request->from_lng_3,

            'to_3'                  => $request->to_3,
            'to_country_3'          => $request->to_country_3,
            'to_country_code_3'     => $request->to_country_code_3,
            'to_lat_3'              => $request->to_lat_3,
            'to_lng_3'              => $request->to_lng_3,

            'departure_date_3'      => $request->departure_date_3,            

            // Trip 4

            'from_4'                => $request->from_4,
            'from_country_4'        => $request->from_country_4,
            'from_country_code_4'   => $request->from_country_code_4,
            'from_lat_4'            => $request->from_lat_4,
            'from_lng_4'            => $request->from_lng_4,

            'to_4'                  => $request->to_4,
            'to_country_4'          => $request->to_country_4,
            'to_country_code_4'     => $request->to_country_code_4,
            'to_lat_4'              => $request->to_lat_4,
            'to_lng_4'              => $request->to_lng_4,

            'departure_date_4'      => $request->departure_date_4,

            // Travel info
            
            'flexibility'           => $request->flexibility,            
            'budget_min'            => $request->budget_min,
            'budget_max'            => $request->budget_max,
            
            'traveling_with'        => $request->traveling_with,            
            'nb_adults'            => $request->nb_adults,
            'nb_kids'              => $request->nb_kids,
            'nb_babies'            => $request->nb_babies,

            'comments'              => $request->comments,

            'flight_class'          => $request->flight_class,
            'meal_type'             => $request->meal_type,
            'seat_type'             => $request->seat_type,
            'flight_comments'       => $request->flight_comments,  

            'accommodation_type'    => $request->accommodation_type,
            'accommodation_rating'  => $request->accommodation_rating,

            'transportation_type'   => $request->transportation_type,
            'car_type'              => $request->car_type,
            'transportation_comments'=> $request->transportation_comments,

            'status'                => 'pending',

            'user_id'               => auth()->user()->id,
        ]);

        if ($request->categories) {
            $enquiry->categories()->attach($request->categories);
        }

        if ($request->to_country_code_1) {
            $country = Country::where('alpha_2_Code', 'LIKE', "%{$request->to_country_code_1}%")->first();            
            $enquiry->countries()->attach($country->id);
        }

        if ($request->categories && $request->to_country_code_1) {
            $agencies = Company::with(['categories', 'countries']);

            $agencies = $agencies->whereHas('countries', function (Builder $query) use ($request) {
                $query->whereIn('alpha_2_Code', [
                        $request->to_country_code_1,
                        $request->to_country_code_2,
                        $request->to_country_code_3,
                        $request->to_country_code_4,
                        ]);                                                            
            })->orWhereHas('categories', function (Builder $query) use ($request) {
                $query->whereIn('category_id', $request->categories);
            });

            $enquiry->agencies()->attach($agencies->get());

        } elseif ($request->categories) {
            $agencies = Company::whereHas('categories', function (Builder $query) use ($request) {
                $query->whereIn('category_id', $request->categories);
            });
            $enquiry->agencies()->attach($agencies->get());
        } elseif ($request->to_country_code_1) {
            $agencies = Company::whereHas('countries', function (Builder $query) use ($request) {
                $query->where('alpha_2_Code', $request->to_country_code_1);
            });
            $enquiry->agencies()->attach($agencies->get());
        }


        Mail::to('no-reply@listo.world')
            ->bcc($enquiry->companiesEmails())
            ->send(new EnquiryCompanies($enquiry));

        Mail::to(auth()->user()->email)->send(new EnquiryUser(auth()->user(), $enquiry));

        session()->flash('success', 'Enquiry sent successfully');

        return redirect(route('profile.enquiries', auth()->user()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Enquiry $enquiry)
    {
        return view('enquiries.show')->with('enquiry', $enquiry);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function edit(Enquiry $enquiry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enquiry $enquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enquiry $enquiry)
    {

        $enquiry->delete();

        session()->flash('success', 'Enquiry deleted successfully');

        return redirect(route('enquiries.index'));
    }
}
