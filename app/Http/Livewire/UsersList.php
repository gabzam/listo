<?php

namespace App\Http\Livewire;
use Livewire\Component;
use Livewire\WithPagination;

class UsersList extends Component
{
    use WithPagination;

    
    public $perPage = 10;
    public $sortField = 'created_at';
    public $sortAsc = true;
    public $search = '';

    public function sortBy($field)    
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc; 
        }else{
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function clear()
    {
        $this->search = '';
    }

    public function render()
    {
        return view('livewire.users-list', [
            'users' => \App\User::search($this->search)
                ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc' )
                ->paginate($this->perPage),            
        ]);
    }
}
