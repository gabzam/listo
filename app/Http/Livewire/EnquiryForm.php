<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EnquiryForm extends Component
{
    public function render()
    {
        return view('livewire.enquiry-form');
    }
}
