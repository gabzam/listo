<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;


class CountryList extends Component
{
    use WithPagination;

    public $countryList;
    
    public function mount($list)
    {
        $this->countryList = $list;
    }

    public function render()
    {
        if ($this->countryList == 'visitedCountries') {
            $list = auth()->user()->visitedCountries()->orderBy('name','asc')->paginate(5);
        }else{
            $list = auth()->user()->bucketListCountries()->orderBy('name','asc')->paginate(5);
        }
        return view('livewire.country-list',['list'=> $list]);
    }
}