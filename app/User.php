<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Country;
use App\Models\Enquiry;
use App\Models\Role;
use Carbon\Carbon;




class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'firstname', 
        'lastname', 
        'email', 
        'password',

        'mobile',
        'phone',
        
        'birthday',
        'gender',
        'picture',
        'profession',
        'biography',
       
        'whatsapp',   
        'viber',     
        'wechat',        
        'telegram',
        'call',
        'sms',
       
        'street',     
        'street_number',
        'address_info',      
        'postal_code',
        'location',  
        'state',     
        'country',  
        'lat',
        'lng',     
       
        'facebook',      
        'linkedin',     
        'instagram',     
        'youtube',    
        'tiktok',      
        'skype',       
        'snapchat',      
        'pinterest',     
       
        'passport1',        
        'passport2',        
        'passport3',        
        'passport4',        
        'passport1_expiry',  
        'passport2_expiry', 
        'passport3_expiry', 
        'passport4_expiry',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'birthday'
    ];

    public static function search($query)
    {
        return empty($query) ? static::query()
            : static::where('firstname', 'like', '%'.$query.'%')
                    ->orWhere('lastname', 'like', '%'.$query.'%')
                    ->orWhere('email', 'like', '%'.$query.'%');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function assignRole($role)
    {
        $this->roles()->sync($role);
    }

    public function hasRole($id)
    {
        return in_array($id, $this->roles()->pluck('id')->toArray()); 
    }

    public function permissions()
    {
        return $this->roles->map->permissions->flatten()->pluck('slug')->unique();
    }

    public function enquiries()
    {
        return $this->hasMany(Enquiry::class);
    }

    public function visitedCountries()
    {
        return $this->belongsToMany(Country::class)->withPivot('status')->wherePivot('status','visited');
    }

    public function bucketListCountries()
    {
        return $this->belongsToMany(Country::class)->withPivot('status')->wherePivot('status','bucketlist');
    }

    public function getAgeAttribute()
    {
        $dob = Carbon::parse($this->birthday);
        return $dob->age;
    }    

    public function getAvatarAttribute()
    {
        
        if (!$this->picture) {
            return $this->gender === 'male' ? asset('img/avatar-male.png') : asset('img/avatar-female.png');
        }

        return $this->picture;
    }
}
