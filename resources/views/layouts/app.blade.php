<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">


    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">
    <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') {{ config('app.name', 'Listo World') }}</title>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.1/dist/alpine.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Share+Tech+Mono&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('head')
    <livewire:styles>
</head>

<body>

    <div id="listo-app">
        @include('partials.navbar')

        <section class="flex flex-row h-screen">
            @if(auth()->user()->hasVerifiedEmail())
            <aside class="flex-none w-64 pb-6 hidden md:block border-r">
                <nav class="mt-20 px-10">
                    @include('partials.navigation', ['responsive' => 'text-base block'])
                </nav>
            </aside>
            @endif

            <main class="flex-1 flex flex-col overflow-hidden bg-gray-100">
                <div class="px-6 py-4 flex-1 overflow-y-scroll">

                    <div class="mt-10 container mx-auto px-2 sm:px-6">

                        @if (session()->has('success'))
                        <div class="mt-20" x-data="{ open: true }" x-show="open">
                            <!--Header Alert-->
                            <div class="alert-banner w-full">
                                <label
                                    class="close cursor-pointer flex items-center justify-between w-full py-4 px-3 bg-green-400 rounded text-white font-bold"
                                    title="close" for="banneralert">
                                    {{ session()->get('success') }}
                                    <button type="button" @click="open = false">
                                        @include('icons.x')
                                    </button>
                                </label>
                            </div>
                        </div>

                        @endif

                        @yield('content')

                    </div>

                    @include('partials.footer')
                </div>
            </main>
        </section>

    </div>

    @yield('scripts')
    <livewire:scripts>
</body>

</html>