@extends('layouts.app')

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        Role
    </h2>
</header>

<div class="rounded py-3">
    <div class="flex flex-wrap mb-4 md:mb-0 py-2 ">
        <div class="w-full sm:w-1/4 md:w-1/6">
            <span class="block uppercase tracking-wide text-xs font-bold mb-2">Name</span>
        </div>
        <div class="w-full sm:w-3/4  md:w-4/6">
            {{ $role->name }}
        </div>
    </div>

    <div class="flex flex-wrap mb-4 md:mb-0 py-2 ">
        <div class="w-full sm:w-1/4 md:w-1/6">
            <span class="block uppercase tracking-wide text-xs font-bold mb-2">Label</span>
        </div>
        <div class="w-full sm:w-3/4  md:w-4/6">
            {{ $role->label }}
        </div>
    </div>

    <div class="flex flex-wrap mb-4 md:mb-0 py-2 ">
        <div class="w-full sm:w-1/4 md:w-1/6">
            <span class="block uppercase tracking-wide text-xs font-bold mb-2">Slug</span>
        </div>
        <div class="w-full sm:w-3/4  md:w-4/6">
            {{ $role->slug }}
        </div>
    </div>

    <div class="flex flex-wrap py-2">
        <div class="w-full sm:w-1/4 md:w-1/6">
            <span class="block uppercase tracking-wide text-xs font-bold mb-2">Permissions</span>
        </div>
        <div class="w-full sm:w-3/4 md:w-4/6">
            <ul>
                @foreach ($role->permissions as $item)
                <li>
                    {{ $item->name }}
                    <span class="text-gray-600 italic ml-2">{{ $item->label ?? '' }}</span>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>



<div class="my-10">
    <a href="{{ route('roles.edit', $role->id)}}"
        class="bg-black text-white py-2 px-3 rounded hover:text-white hover:bg-pink-700 leading-none">Edit
        role</a>
    <form action="{{ route('roles.destroy', $role->id )}}" method="post" class="inline">
        @csrf
        @method('delete')
        <button type="submit"
            class="bg-red-700 text-white py-2 px-3 rounded hover:text-white hover:bg-pink-700 leading-none">Delete</button>
    </form>
</div>

@endsection