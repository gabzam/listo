@extends('layouts.app')

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate text-gray-800">
        Create Role
    </h2>
</header>
<form action="{{ route('roles.store') }}" method="post">
    @csrf

    <div class="w-full md:w-1/2 mb-6">
        <label class="block uppercase tracking-wide text-gray-900 text-xs font-bold mb-2" for="grid-first-name">
            Name
        </label>
        <input
            class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-pink-700 @error('name') border-red-500 field-error @enderror"
            id="name" name="name" type="text" placeholder="ex: admin" value="{{ old('value') }}">
        @error('name')
        <span class="text-red-600 text-sm italic" role="alert">
            {{ $message }}
        </span>
        @enderror
    </div>

    <div class="w-full md:w-1/2 mb-6">
        <label class="block uppercase tracking-wide text-gray-900 text-xs font-bold mb-2" for="grid-first-name">
            Label
        </label>
        <input
            class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-pink-700 "
            id="label" name="label" type="text" placeholder="ex: administrator of website, full access"
            value="{{ old('value') }}">
    </div>

    <div class="w-full md:w-1/2 mb-6">
        <label class="block uppercase tracking-wide text-gray-900 text-xs font-bold mb-2" for="grid-first-name">
            Permissions
        </label>
        @foreach ($permissions as $p)
        <div>
            <input type="checkbox" name="permissions[]" id="permissions" class="mr-2 leading-tight"
                value="{{ $p->id }}">
            <span class="text-sm">
                {{ $p->name }}
            </span>
        </div>
        @endforeach
    </div>

    <button type="submit" class="bg-black rounded px-3 py-2 text-white hover:bg-pink-700 focus:outline-none">
        Create Role
    </button>
</form>
@endsection