@extends('layouts.app')

@section('content')
<div class="my-20 flex justify-center">
    <div class="w-1/2">
        <iframe src="https://giphy.com/embed/7WwVYKDMt5khG" width="100%" height="258" frameBorder="0"
            class="giphy-embed" allowFullScreen></iframe>
        <p class="text-center"><a href="https://giphy.com/gifs/robert-eggers-7WwVYKDMt5khG">via GIPHY</a></p>
    </div>
</div>
@endsection