@extends('layouts.app')

@section('content')

<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate text-gray-700">
        New Tender
    </h2>
</header>

@livewire('enquiry-form')

@if ($errors->any())
<div class="alert alert-banner w-full mb-6">
    <ul class="w-full py-4 px-3 bg-red-600 shadow text-white font-bold list-disc pl-6">
        @foreach ($errors->all() as $er)
        <li>{{ $er }}</li>
        @endforeach
    </ul>
</div>
@endif

<form class="w-full kirby-form mb-10" action="{{ route('enquiries.store') }}" method="POST" x-data="counter()">
    @csrf

    <div class="w-full md:w-1/4 mb-6">
        <label class="block uppercase tracking-wide text-xs font-bold mb-2" for="travel_type">
            Travel type
        </label>
        <div class="relative inline-flex">

            <label for="round-trip" class="my-1 mt-0 mr-3">
                <input x-on:click="selected = 'round-trip', count = 0" id="round-trip" type="radio" name="travel_type"
                    value="round-trip" checked>
                <span class="font-normal">Round trip</span>
            </label>

            <label for="round-trip" class="my-1 mt-0 mx-3">
                <input x-on:click="selected = 'one-way'" id="one-way" type="radio" name="travel_type" value="one-way">
                <span class="font-normal">One way</span>
            </label>
            <label for="round-trip" class="my-1 mt-0 ml-3">
                <input x-on:click="selected = 'multi-cities'" id="multi-cities" type="radio" name="travel_type"
                    value="multi-cities">
                <span class="font-normal">Multi-cities</span>
            </label>
        </div>
    </div>

    {{-- Departure & Destination --}}
    @include('enquiries.form.places')

    {{-- Options --}}
    <div class="my-6">
        @include('enquiries.form.options')
    </div>

    @include('enquiries.form.comments')

    <br>

    @include('enquiries.form.themes')

    <br>

    <div x-data="{ flight: false, transportation: false, accomodation: false }" class="mb-10">

        <div class="inline-flex mr-5">
            <input type="checkbox" name="flight-options" id="flight-options" x-on:click="flight = !flight">
            <label for="flight-options" class="ml-2"><span class="font-normal text-black">Flight options</span></label>
        </div>

        <div class="inline-flex mr-5">
            <input type="checkbox" name="accomodation-options" id="accomodation-options"
                x-on:click="accomodation = !accomodation">
            <label for="accomodation-options" class="ml-2">
                <span class="font-normal text-black">Accomodations options</span>
            </label>
        </div>

        {{-- <div class="inline-flex">
            <input type="checkbox" name="transportation-options" id="transportation-options"
                x-on:click="transportation = !transportation">
            <label for="transportation-options" class="ml-2">
                <span class="font-normal text-black">Transportation options</span>
            </label>
        </div> --}}

        <div x-show="flight" @click.away="flight = flight">
            @include('enquiries.form.flight')
        </div>

        <div x-show="accomodation" @click.away="accomodation = accomodation">
            @include('enquiries.form.accommodation')
        </div>

        {{-- <div x-show="transportation" @click.away="transportation = transportation">
            @include('enquiries.form.transportation')
        </div> --}}

    </div>

    <button type="submit"
        class="uppercase px-4 py-2 border border-transparent leading-5 font-medium text-white bg-pink-700 hover:bg-gray-800 focus:outline-none transition ease-in-out duration-150">
        Request a bid
    </button>
</form>

@endsection