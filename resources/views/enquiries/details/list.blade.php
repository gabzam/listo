<div id="enquiry-list" class="border rounded">
    <div class="flex flex-wrap w-full bg-gray-100 px-3 py-2 border-b items-center">
        <div class="md:w-1/5">
            <span class="block text-sm uppercase text-gray-700 font-semibold">from > to</span>
        </div>
        <div class="hidden md:inline w-1/5">
            <span class="block text-sm uppercase text-gray-700 font-semibold">Date(s)</span>
        </div>
        <div class="hidden md:inline w-1/5">
            <span class="block text-sm uppercase text-gray-700 font-semibold">Sent</span>
        </div>
        <div class="hidden md:inline w-1/5">
            <span class="block text-sm uppercase text-gray-700 font-semibold">Status</span>
        </div>
        <div class="w-full md:w-1/5">
            <div class="text-right">
                <a href="{{route('enquiries.create')}}"
                    class="bg-indigo-600 py-1 px-2 text-gray-100 hover:bg-pink-700 hover:text-white rounded text-sm">
                    @include('icons.add') <span class="ml-2">Request offer</span>
                </a>
            </div>
        </div>
    </div>
    <ul class="data-list">
        @forelse ($enquiries as $enquiry)
        <li>
            {{-- {{$enquiry}} --}}
            <div class="flex flex-wrap items-center text-gray-700">
                <div class="w-full md:w-1/5">
                    <p>{{ $enquiry->from_1 }} > {{ $enquiry->to_1 }}</p>
                    @if ( $enquiry->from_2 )
                    <p>{{ $enquiry->from_2 }} > {{ $enquiry->to_2 }}</p>
                    @endif
                    @if ( $enquiry->from_3)
                    <p>{{ $enquiry->from_3 }} > {{ $enquiry->to_3 }}</p>
                    @endif
                    @if ( $enquiry->from_4 )
                    <p>{{ $enquiry->from_4 }} > {{ $enquiry->to_4 }}</p>
                    @endif
                </div>
                <div class="w-full md:w-1/5">
                    <p class="text-sm">
                        {{ $enquiry->departure_date_1 }}
                        {{$enquiry->return_date_1 ? '- ' . $enquiry->return_date_1 : ''}}
                    </p>
                    @if ($enquiry->departure_date_2)
                    <p class="text-sm">
                        {{ $enquiry->departure_date_2 }}
                    </p>
                    @endif
                    @if ($enquiry->departure_date_3)
                    <p class="text-sm">
                        {{ $enquiry->departure_date_3 }}
                    </p>
                    @endif
                    @if ($enquiry->departure_date_4)
                    <p class="text-sm">
                        {{ $enquiry->departure_date_4 }}
                    </p>
                    @endif
                </div>
                <div class="w-full md:w-1/5">
                    <span class="text-sm">{{ $enquiry->created_at }}</span>
                </div>
                <div class="w-full md:w-1/5">
                    <span
                        class="text-sm bg-yellow-300 text-yellow-900 py-1 px-2 rounded-full">{{ $enquiry->status }}</span>
                </div>
                <div class="w-full md:w-1/5 text-right inline-flex justify-end">
                    <a href="{{ route('enquiries.show', $enquiry->id )}}"
                        class="text-gray-600 inline p-2 text-sm hover:text-white hover:bg-indigo-700 rounded-full">
                        @include('icons.view')
                    </a>
                    <form action="{{ route('enquiries.destroy', $enquiry->id) }}" method="post" class="inline m-0 p-0">
                        @csrf
                        @method('DELETE')
                        <button type="submit"
                            class="text-gray-600 inline p-2 text-sm hover:text-white hover:bg-red-600 rounded-full"
                            style="line-height:normal !important;">
                            @include('icons.trash')
                        </button>
                    </form>

                </div>

            </div>
        </li>
        @empty

        <li>You have not sent any enquiry, start now!</li>

        @endforelse
    </ul>
</div>
{{ $enquiries->appends([ 'search' => request()->query('search') ])->links() }}