<h2 class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">Request from:</h2>
<span class="block mb-2">
    <strong>{{ $enquiry->user->firstname}} {{ $enquiry->user->lastname}}</strong>
    ({{ $enquiry->user->age}} years old)
</span>

<table>
    <tr>
        <td class="font-bold">email:</td>
        <td>{{ $enquiry->user->email }}</td>
    </tr>
    <tr>
        <td class="font-bold">mobile:</td>
        <td>{{ $enquiry->user->mobile }}</td>
    </tr>

    <tr>
        <td class="font-bold">phone:</td>
        <td>{{ $enquiry->user->phone }}</td>
    </tr>

    <tr>
        <td class="font-bold">Phone call:</td>
        <td>{{ $enquiry->user->call ? 'Yes' : 'No' }}</td>
    </tr>

    <tr>
        <td class="font-bold">SMS:</td>
        <td>{{ $enquiry->user->sms ? 'Yes' : 'No' }}</td>
    </tr>

    <tr>
        <td class="font-bold">Whatsapp:</td>
        <td>{{ $enquiry->user->whatsapp ? 'Yes' : 'No' }}</td>
    </tr>

    <tr>
        <td class="font-bold">Viber:</td>
        <td>{{ $enquiry->user->viber ? 'Yes' : 'No' }}</td>
    </tr>

    <tr>
        <td class="font-bold">WeChat:</td>
        <td>{{ $enquiry->user->wechat ? 'Yes' : 'No' }}</td>
    </tr>

    <tr>
        <td class="font-bold">Telegram:</td>
        <td>{{ $enquiry->user->telegram ? 'Yes' : 'No' }}</td>
    </tr>

    <tr>
        <td class="font-bold">Facebook:</td>
        <td>{{ $enquiry->user->facebook }}</td>
    </tr>

    <tr>
        <td class="font-bold">Instagram:</td>
        <td>{{ $enquiry->user->instagram }}</td>
    </tr>

    <tr>
        <td class="font-bold">Tiktok:</td>
        <td>{{ $enquiry->user->tiktok }}</td>
    </tr>

    <tr>
        <td class="font-bold">Linkedin:</td>
        <td>{{ $enquiry->user->linkedin }}</td>
    </tr>

    <tr>
        <td class="font-bold">Youtube:</td>
        <td>{{ $enquiry->user->youtube }}</td>
    </tr>

    <tr>
        <td class="font-bold">Twitter:</td>
        <td>{{ $enquiry->user->twitter }}</td>
    </tr>

    <tr>
        <td class="font-bold">Pinterest:</td>
        <td>{{ $enquiry->user->pinterest }}</td>
    </tr>

    <tr>
        <td class="font-bold">Skype:</td>
        <td>{{ $enquiry->user->skype }}</td>
    </tr>

    <tr>
        <td class="font-bold">Snapchat:</td>
        <td>{{ $enquiry->user->snapchat }}</td>
    </tr>

</table>