@section('head')
<style>
    #chartdiv {
        width: 100%;
        height: 555px;
    }
</style>
@endsection

<div id="chartdiv"></div>

@section('scripts')

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/maps.js"></script>
<script src="https://www.amcharts.com/lib/4/geodata/worldLow.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
    am4core.ready(function() {
    
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end
    
    // Create map instance
    var chart = am4core.create("chartdiv", am4maps.MapChart);
    chart.geodata = am4geodata_worldLow;
    chart.projection = new am4maps.projections.Miller();
    chart.homeZoomLevel = 1;

    
    // Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
    polygonSeries.useGeodata = true;
    polygonSeries.mapPolygons.template.fill = chart.colors.getIndex(0).lighten(0.5);
    polygonSeries.mapPolygons.template.nonScalingStroke = true;
    polygonSeries.exclude = ["AQ"];
    
    // Add line bullets
    var cities = chart.series.push(new am4maps.MapImageSeries());
    cities.mapImages.template.nonScaling = true;
    
    var city = cities.mapImages.template.createChild(am4core.Circle);
    city.radius = 6;
    city.fill = chart.colors.getIndex(0).brighten(-0.2);
    city.strokeWidth = 2;
    city.stroke = am4core.color("#fff");
    
    function addCity(coords, title) {
        var city = cities.mapImages.create();
        city.latitude = coords.latitude;
        city.longitude = coords.longitude;
        city.tooltipText = title;
        return city;
    }
    
    var city1 = addCity({ "latitude": {{ $enquiry->from_lat_1 }}, "longitude": {{ $enquiry->from_lng_1 }} }, "{{ $enquiry->from_1 }}" );
    var city2 = addCity({ "latitude": {{ $enquiry->to_lat_1 }}, "longitude": {{ $enquiry->to_lng_1 }} }, "{{ $enquiry->to_1 }}");

    let fromLat2 = {{ $enquiry->from_lat_2 ?? 'undefined' }};
    let fromLng2 = {{ $enquiry->from_lng_2 ?? 'undefined' }};
    let toLat2 = {{ $enquiry->to_lat_2 ?? 'undefined' }};
    let toLng2 = {{ $enquiry->to_lng_2 ?? 'undefined' }};
    
    if(typeof fromLat2 !== "undefined"){          
        var city3 = addCity({ "latitude": fromLat2, "longitude": fromLng2 }, "{{ $enquiry->from_2 }}" );                
        var city4 = addCity({ "latitude": toLat2, "longitude": toLng2 }, "{{ $enquiry->to_location_2 }}");
    }

    let fromLat3 = {{ $enquiry->from_lat_3 ?? 'undefined' }};
    let fromLng3 = {{ $enquiry->from_lng_3 ?? 'undefined' }};
    let toLat3 = {{ $enquiry->to_lat_3 ?? 'undefined' }};
    let toLng3 = {{ $enquiry->to_lng_3 ?? 'undefined' }};
    if(typeof fromLat3 !== 'undefined'){
        var city5 = addCity({ "latitude": fromLat3, "longitude": fromLng3 }, "{{ $enquiry->from_3 }}" );                
        var city6 = addCity({ "latitude": toLat3, "longitude": toLng3 }, "{{ $enquiry->to_3 }}");
    }

    let fromLat4 = {{ $enquiry->from_lat_4 ?? 'undefined' }};
    let fromLng4 = {{ $enquiry->from_lng_4 ?? 'undefined' }};
    let toLat4 = {{ $enquiry->to_lat_4 ?? 'undefined' }};
    let toLng4 = {{ $enquiry->to_lng_4 ?? 'undefined' }};
    if(typeof fromLat4 !== 'undefined'){
        var city7 = addCity({ "latitude": fromLat4, "longitude": fromLng4 }, "{{ $enquiry->from_4 }}" );                
        var city8 = addCity({ "latitude": toLat4, "longitude": toLng4 }, "{{ $enquiry->to_4 }}");
    }
    

    // Add lines
    var lineSeries = chart.series.push(new am4maps.MapArcSeries());
    lineSeries.mapLines.template.line.strokeWidth = 2;
    lineSeries.mapLines.template.line.strokeOpacity = 0.5;
    lineSeries.mapLines.template.line.stroke = city.fill;
    lineSeries.mapLines.template.line.nonScalingStroke = true;
    lineSeries.mapLines.template.line.strokeDasharray = "1,1";
    lineSeries.zIndex = 10;
    
    var shadowLineSeries = chart.series.push(new am4maps.MapLineSeries());
    shadowLineSeries.mapLines.template.line.strokeOpacity = 0;
    shadowLineSeries.mapLines.template.line.nonScalingStroke = true;
    shadowLineSeries.mapLines.template.shortestDistance = false;
    shadowLineSeries.zIndex = 5;
    
    function addLine(from, to) {
        var line = lineSeries.mapLines.create();
        line.imagesToConnect = [from, to];
        line.line.controlPointDistance = -0.3;
    
        var shadowLine = shadowLineSeries.mapLines.create();
        shadowLine.imagesToConnect = [from, to];
    
        return line;
    }
    
    addLine(city1, city2);

    if(typeof fromLat2 !== 'undefined'){
        addLine(city3, city4);     
    }

    if(typeof fromLat3 !== 'undefined'){
        addLine(city5, city6);
    }

    if(typeof fromLat4 !== 'undefined'){
        addLine(city7, city8);
    }

    
    // Add plane
    var plane = lineSeries.mapLines.getIndex(0).lineObjects.create();
    plane.position = 0;
    plane.width = 48;
    plane.height = 48;
    
    plane.adapter.add("scale", function(scale, target) {
        return 0.5 * (1 - (Math.abs(0.5 - target.position)));
    })
    
    var planeImage = plane.createChild(am4core.Sprite);
    planeImage.scale = 0.08;
    planeImage.horizontalCenter = "middle";
    planeImage.verticalCenter = "middle";
    planeImage.path = "m2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47";
    planeImage.fill = chart.colors.getIndex(2).brighten(-0.2);
    planeImage.strokeOpacity = 0;
    
    var shadowPlane = shadowLineSeries.mapLines.getIndex(0).lineObjects.create();
    shadowPlane.position = 0;
    shadowPlane.width = 48;
    shadowPlane.height = 48;
    
    var shadowPlaneImage = shadowPlane.createChild(am4core.Sprite);
    shadowPlaneImage.scale = 0.05;
    shadowPlaneImage.horizontalCenter = "middle";
    shadowPlaneImage.verticalCenter = "middle";
    shadowPlaneImage.path = "m2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47";
    shadowPlaneImage.fill = am4core.color("#000");
    shadowPlaneImage.strokeOpacity = 0;
    
    shadowPlane.adapter.add("scale", function(scale, target) {
        target.opacity = (0.6 - (Math.abs(0.5 - target.position)));
        return 0.5 - 0.3 * (1 - (Math.abs(0.5 - target.position)));
    })
    
    // Plane animation
    var currentLine = 0;
    var direction = 1;
    let travel_type = "{{ $enquiry->travel_type }}";
    function flyPlane() {
    
        // Get current line to attach plane to
        plane.mapLine = lineSeries.mapLines.getIndex(currentLine);
        plane.parent = lineSeries;
        shadowPlane.mapLine = shadowLineSeries.mapLines.getIndex(currentLine);
        shadowPlane.parent = shadowLineSeries;
        shadowPlaneImage.rotation = planeImage.rotation;
    
        // Set up animation
        var from, to;
        var numLines = lineSeries.mapLines.length;
        if(travel_type == 'round-trip'){
            if (direction == 1) {
                from = 0
                to = 1;            
                if (planeImage.rotation != 0) {
                    planeImage.animate({ to: 0, property: "rotation" }, 1000).events.on("animationended", flyPlane);
                    return;
                }
            }
            else {
                from = 1;
                to = 0;
                if (planeImage.rotation != 180) {
                    planeImage.animate({ to: 180, property: "rotation" }, 1000).events.on("animationended", flyPlane);
                    return;
                }
            }
        }else{
            from = 0
            to = 1;            
            if (planeImage.rotation != 0) {
                planeImage.animate({ to: 0, property: "rotation" }, 1000).events.on("animationended", flyPlane);
                return;
            }  
        }
    
        // Start the animation
        var animation = plane.animate({
            from: from,
            to: to,
            property: "position"
        }, 5000, am4core.ease.sinInOut);
        animation.events.on("animationended", flyPlane)
        /*animation.events.on("animationprogress", function(ev) {
          var progress = Math.abs(ev.progress - 0.5);
          //console.log(progress);
          //planeImage.scale += 0.2;
        });*/
    
        shadowPlane.animate({
            from: from,
            to: to,
            property: "position"
        }, 5000, am4core.ease.sinInOut);
    
        // Increment line, or reverse the direction
        currentLine += direction;
        if (currentLine < 0) {
            currentLine = 0;
            direction = 1;
        }
        else if ((currentLine + 1) > numLines) {
            currentLine = numLines - 1;
            direction = -1;
        }
    
    }
    
    // Go!
    flyPlane();
    
    }); // end am4core.ready()
</script>

<!-- HTML -->
@endsection