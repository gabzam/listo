@isset ($enquiry->accommodation_type)
<h1 class="font-bold text-3xl mb-3 text-gray-800">
    Accommodation options
</h1>

<div class="flex flex-wrap -mx-3 mb-3">
    <div class="w-full md:w-1/3 px-3 mb-3">
        <div class="text-center rounded shadow pb-5 pt-8 text-gray-800">
            <span class="inline-flex capitalize text-3xl">@include('icons.buildings',['style'=>'h-10 w-10 mr-2'])
                {{ $enquiry->accommodation_type }}
            </span>
            <span class="text-md text-gray-600 block">Accommodation type</span>
        </div>
    </div>
    <div class="w-full md:w-1/3 px-3 mb-3">
        <div class="text-center rounded shadow pb-5 pt-8 text-gray-800">
            <span class="inline-flex capitalize text-3xl">
                @include('icons.star',['style'=>'h-10 w-10 mr-2'])
                {{ $enquiry->accommodation_rating }} stars
            </span>
            <span class="text-md text-gray-600 block">Rating</span>
        </div>
    </div>
    <div class="w-full md:w-1/3 px-3 mb-3">
    </div>
</div>
@endisset