<div class="flex flex-wrap -mx-3 text-gray-700">
    <div class="w-full md:w-1/3 px-3 mb-3">
        <div class="text-center roundedshadow py-5 text-3xl">
            <span class="inline-flex capitalize">@include('icons.flight-class',['style'=>'h-10 w-10 mr-2'])
                {{ $enquiry->flight_class }}
            </span>
        </div>
    </div>
    <div class="w-full md:w-1/3 px-3 mb-3">
        <div class="text-center roundedshadow py-5 text-3xl">
            <span class="inline-flex capitalize">@include('icons.gastronomy',['style'=>'h-10 w-10 mr-2'])
                {{ $enquiry->meal_type }}
            </span>
        </div>
    </div>
    <div class="w-full md:w-1/3 px-3 mb-3">
        <div class="text-center roundedshadow py-5 text-3xl">
            <span class="inline-flex capitalize">@include('icons.seat',['style'=>'h-10 w-10 mr-2'])
                {{ $enquiry->seat_type }}
            </span>
        </div>
    </div>
</div>

<p class="text-md uppercase text-gray-600 text-center">Flight options</p>