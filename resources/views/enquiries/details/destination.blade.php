<table class="w-full">
    <tr>
        <td class="font-bold text-gray-800 text-lg text-center" width="20%">
            @switch($number)
            @case(2)
            {{ $enquiry->from_2 }}
            @break
            @case(3)
            {{ $enquiry->from_3 }}
            @break
            @case(4)
            {{ $enquiry->from_4 }}
            @break
            @default
            {{ $enquiry->from_1 }}
            {{-- {{ $enquiry->from_country_1 }}
            {{ $enquiry->from_country_code_1 }}
            {{ $enquiry->from_lat_1 }}
            {{ $enquiry->from_lng_1 }} --}}
            @endswitch
        </td>

        <td class="text-center" width="60%">
            <div class="line-container">
                <span class="line"></span>
                <label class="inline-flex"">
                    @switch($number)
                        @case(2)
                        &nbsp;&nbsp; {{ $enquiry->departure_date_2 }} &nbsp;&nbsp;
                            @break
                        @case(3)
                        &nbsp;&nbsp; {{ $enquiry->departure_date_3 }} &nbsp;&nbsp;
                            @break
                        @case(4)
                        &nbsp;&nbsp; {{ $enquiry->departure_date_4 }} &nbsp;&nbsp;
                            @break
                        @default
                        &nbsp;&nbsp; {{ $enquiry->departure_date_1 }} &nbsp;&nbsp;        
                    @endswitch                    
                    @include('icons.plane', ['style'=>'h-5 w-5 mr-2 text-gray-800'])
                </label>
                <span class=" line arrow-right"></span>
            </div>


            @if ($enquiry->return_date_1)
            <div class="line-container">
                <span class="line arrow-left"></span>
                <label class="inline-flex"">
                    @include('icons.plane', ['style'=>'h-5 w-5 mr-2 text-gray-800 transform rotate-180']){{ $enquiry->return_date_1 }} &nbsp;&nbsp;                    
                </label>
                <span class=" line"></span>
            </div>
            @endif

        </td>
        <td class=" font-bold text-gray-800 text-lg text-center" width="20%">
            @switch($number)
            @case(2)
            {{ $enquiry->to_2 }}
            @break
            @case(3)
            {{ $enquiry->to_3 }}
            @break
            @case(4)
            {{ $enquiry->to_4 }}
            @break
            @default
            {{ $enquiry->to_1 }}
            {{-- {{ $enquiry->to_country_1 }}
            {{ $enquiry->to_country_code_1 }}
            {{ $enquiry->to_lat_1 }}
            {{ $enquiry->to_lng_1 }} --}}
            @endswitch


        </td>
    </tr>
</table>