@extends('layouts.app')


@section('content')

<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate text-gray-800">
        Enquiries
    </h2>
</header>

@include('shared.search',['action' => 'enquiries.index'])


@include('enquiries.details.list')

@endsection