<div class="mt-3 mb-10">
    <div class="flex flex-wrap">
        <div class="w-full md:w-1/3 pr-2 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-xs font-bold mb-2" for="flight_class">
                Flight class
            </label>
            <div class="relative">
                <select
                    class="block appearance-none w-full bg-white border border-gray-200 py-3 px-4 pr-8 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="flight_class" name="flight_class">
                    <option value="Economy">Economy</option>
                    <option value="Premium Economy">Premium Economy</option>
                    <option value="Business Class">Business Class</option>
                    <option value="First Class">First Class</option>
                    <option value="Private Jet">Private jet</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                </div>
            </div>
        </div>

        {{-- seat_type --}}
        <div class="w-full md:w-1/3 pr-2 mb-6 md:mb-0">
            <label for="seat_type">
                Seat Type
            </label>
            <div class="relative">
                <select id="seat_type" name="seat_type">
                    <option value="window">Window</option>
                    <option value="aisle">Aisle</option>
                    <option value="exit Row">Exit Row</option>
                    <option value="legroom espace">Legroom espace</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                </div>
            </div>
        </div>

        {{-- meal_type --}}
        <div class="w-full md:w-1/3 pr-2 mb-6 md:mb-0">
            <label for="meal_type">
                Meal Type
            </label>
            <div class="relative">
                <select id="meal_type" name="meal_type">
                    <option value="regular">Regular</option>
                    <option value="vegetarian">Vegetarian</option>
                    <option value="vegan">Vegan</option>
                    <option value="gluten Free">Gluten free</option>
                    <option value="lactose Free">Lactose free</option>
                    <option value="diabetic">For diabetics</option>
                    <option value="low Calories">Low calories</option>
                    <option value="low Fat">Low fat</option>
                    <option value="low Salt">Low Salt</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-4 mb-6">
        <label class="block uppercase tracking-wide text-xs font-bold mb-2" for="flight_comments">
            Flight Comments
        </label>
        <textarea
            class="block appearance-none w-full bg-white border border-gray-200 py-3 px-4 pr-8 leading-tight focus:outline-none focus:border-gray-500"
            name="flight_comments" id="flight_comments" cols="30" rows="2"></textarea>
    </div>

</div>