<div class="mt-3 mb-10">
    <div class="flex flex-wrap">
        <div class="w-full md:w-1/3 pr-2 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-xs font-bold mb-2" for="accommodation_type">
                Accommodation Type
            </label>
            <div class="relative">
                <select
                    class="block appearance-none w-full bg-white border border-gray-200 py-3 px-4 pr-8 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="accommodation_type" name="accommodation_type">
                    <option disable selected value>Select Accommodation</option>
                    <option value="Hotel">Hotel</option>
                    <option value="Apartment">Apartment</option>
                    <option value="House/Villa">House</option>
                    <option value="Hostel">First Class</option>
                    <option value="Guest House">Guest House</option>
                    <option value="Bed & Breakfast">Bed & Breakfast</option>
                    <option value="Camping">Camping</option>
                    <option value="Boat">Boat</option>
                    <option value="Resort">Resort</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                </div>
            </div>
        </div>

        {{-- seat_type --}}
        <div class="w-full md:w-1/3 pr-2 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-xs font-bold mb-2" for="accommodation_rating">
                Rating
            </label>
            <div class="relative">
                <select id="accommodation_rating" name="accommodation_rating">
                    <option disable selected value>Select Rating</option>
                    <option value="1">1 Star</option>
                    <option value="2">2 Stars</option>
                    <option value="3">3 Stars</option>
                    <option value="4">4 Stars</option>
                    <option value="5">5 Stars</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                </div>
            </div>
        </div>

        {{-- meal_type --}}
        <div class="w-full md:w-1/3 pr-2 mb-6 md:mb-0">

        </div>
    </div>
</div>