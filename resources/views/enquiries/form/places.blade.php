@section('head')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@include('enquiries.form._destination', ['number'=> '1'])

<div x-show="selected == 'multi-cities'">
  <hr><br>
  @include('enquiries.form._destination', ['number'=> '2'])
</div>

<div x-show="count > 0">
  <hr><br>
  @include('enquiries.form._destination', ['number'=> '3'])
</div>

<div x-show="count > 1">
  <hr><br>
  @include('enquiries.form._destination', ['number'=> '4'])
  <p class="text-sm text-gray-800 -mt-4 pb-5">
    For more than 4 connections please consider explaining the whole trip in the
    comments section below with as many details as you can. <br> Our travel partners will do their best to find you
    solutions that fits you.
  </p>
</div>

<div x-show="selected == 'multi-cities'">
  <button x-show="count < 2" type="button" x-on:click="increment()"
    class="bg-indigo-600 text-gray-100 hover:bg-pink-700 hover:text-white px-2 py-1 rounded-full inline-flex items-center text-sm">
    @include('icons.add') <span class="ml-2">Add Trip</span>
  </button>
  <button x-show="count > 0" type="button" x-on:click="decrement()"
    class="bg-indigo-600 text-gray-100 hover:bg-pink-700 hover:text-white px-2 py-1 rounded-full inline-flex items-center text-sm">
    @include('icons.minus-circle') <span class="ml-2">Remove Trip</span>
  </button>
</div>

@section('scripts')

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdn.jsdelivr.net/npm/places.js@1.18.1"></script>
<script>
  function counter() {
    return {
      selected: 'round-trip', 
      count: 0,      
      increment() {
        this.count++;        
      },
      decrement() {
        this.count--;                  
      }          
    };
  }

  flatpickr("#departure_date_1",{
    minDate: new Date().fp_incr(2)
  });
  flatpickr("#departure_date_2",{
    minDate: new Date().fp_incr(3)
  });

  flatpickr("#departure_date_3",{
    minDate: new Date().fp_incr(4)
  });

  flatpickr("#departure_date_4",{
    minDate: new Date().fp_incr(5)
  });
  flatpickr("#return_date_1",{
    minDate: new Date().fp_incr(3)
  });
  
  function autocomplete(number) {
    
    let departure_location = places({
      appId: "{{ config('services.algolia_places.id')}}",
      apiKey: "{{ config('services.algolia_places.key') }}",
      container: document.querySelector('#from_' + number ),
      templates: {
        value: function(suggestion) {
          return suggestion.name;
        }
      }
    }).configure({
      type: 'city',
      aroundLatLngViaIP: false
    });

    let destination_location = places({
      appId: "{{ config('services.algolia_places.id')}}",
      apiKey: "{{ config('services.algolia_places.key') }}",
      container: document.querySelector('#to_' + number),
      templates: {
        value: function(suggestion) {
          return suggestion.name;
        }
      }
    }).configure({
      type: 'city',
      aroundLatLngViaIP: false
    });

    departure_location.on('change', function resultSelected(e) {
      document.querySelector('#from_country_' + number).value = e.suggestion.country || '';
      document.querySelector('#from_country_code_' + number).value = e.suggestion.countryCode || '';
      document.querySelector('#from_lat_' + number).value = e.suggestion.latlng.lat || '';
      document.querySelector('#from_lng_' + number).value = e.suggestion.latlng.lng || '';
    });

    destination_location.on('change', function resultSelected(e) {
      document.querySelector('#to_country_' + number).value = e.suggestion.country || '';
      document.querySelector('#to_country_code_' + number).value = e.suggestion.countryCode || '';
      document.querySelector('#to_lat_' + number).value = e.suggestion.latlng.lat || '';
      document.querySelector('#to_lng_' + number).value = e.suggestion.latlng.lng || '';
    });
};

autocomplete(1);
autocomplete(2);
autocomplete(3);
autocomplete(4);

</script>

@endsection