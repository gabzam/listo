@if ($categories->count() > 0)
<div class="mb-6">
    <label for="categories">Purpose of this trip (Theme)</label>
    <div class="flex flex-wrap">
        @foreach ($categories as $cat)
        <div class="w-1/2 sm:w-1/3 md:w-1/4 lg:w-1/5 inline-flex items-center py-1">
            <input type="checkbox" name="categories[]" class="mr-2 leading-tight" value="{{$cat->id}}">
            <span class="text-sm inline-flex items-center ml-1">
                @include('icons.'.$cat->icon)
                <span>{{ $cat->name }}</span>
            </span>
        </div>
        @endforeach
    </div>
</div>
@endif