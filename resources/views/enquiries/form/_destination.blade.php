<div class="flex flex-wrap -mx-3">

    <div class="w-full md:w-1/3 px-3 mb-6">
        <label for="from_{{$number}}">{{ __('From') }}</label>
        <input id="from_{{$number}}" name="from_{{$number}}" type="text"
            class="form-control @error('from_' . $number ) field-error @enderror" value="{{ old('from_' . $number )}}">

        @error('from_'. $number)
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
        @enderror

        <input type="hidden" name="from_country_{{$number}}" id="from_country_{{$number}}"
            value="{{ old('from_country_'. $number )}}">
        <input type="hidden" name="from_country_code_{{$number}}" id="from_country_code_{{$number}}"
            value="{{ old('from_country_code_' . $number)}}">
        <input type="hidden" name="from_lat_{{$number}}" id="from_lat_{{$number}}"
            value="{{ old('from_lat_' . $number)}}">
        <input type="hidden" name="from_lng_{{$number}}" id="from_lng_{{$number}}"
            value="{{ old('from_lng_' . $number)}}">
    </div>

    <div class="w-full md:w-1/3 px-3 mb-6">
        <label for="to_{{$number}}">{{ __('To') }}</label>
        <input id="to_{{$number}}" name="to_{{$number}}" type="text" value="{{ old('to_' . $number)}}">

        <input type="hidden" name="to_country_{{$number}}" id="to_country_{{$number}}"
            value="{{ old('to_country_' . $number)}}">
        <input type="hidden" name="to_country_code_{{$number}}" id="to_country_code_{{$number}}"
            value="{{ old('to_country_code_' . $number)}}">
        <input type="hidden" name="to_lat_{{$number}}" id="to_lat_{{$number}}" value="{{ old('to_lat_' . $number)}}">
        <input type="hidden" name="to_lng_{{$number}}" id="to_lng_{{$number}}" value="{{ old('to_lng_' . $number)}}">
    </div>

    <div class="w-full md:w-1/3 px-3 mb-6">
        <div class="flex flex-wrap -mx-3">
            <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-xs font-bold mb-2" for="departure_date_{{$number}}">
                    Departure date
                </label>
                <input type="date" placeholder="Select date"
                    class="w-full border bg-white border-gray-200 py-3 px-4 leading-tight appearance-none block focus:outline-none focus:border-gray-500"
                    id="departure_date_{{$number}}" name="departure_date_{{$number}}">
            </div>
            <div class="w-full md:w-1/2 px-3">
                <div x-show="selected == 'round-trip'">
                    <label for="return_date_{{$number}}">Return date</label>
                    <input type="date" placeholder="Select a date"
                        class="w-full border bg-white border-gray-200 py-3 px-4 leading-tight appearance-none block focus:outline-none focus:border-gray-500"
                        id="return_date_{{$number}}" name="return_date_{{$number}}">
                </div>
            </div>
        </div>
    </div>
</div>