<div class="flex flex-wrap -mx-3">
    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
        <label for="flexibility">
            Flexibility
        </label>
        <div class="relative">
            <select id="flexibility" name="flexibility">
                <option value="Exact dates">Exact dates</option>
                <option value="+/- 3 days">+/- 3 days</option>
                <option value="+/- 1 week">+/- 1 week</option>
                <option value="+/- 2 weeks">+/- 2 weeks</option>
                <option value="+/- 1 month">+/- 1 month</option>
                <option value="Weekend">Weekend</option>
                <option value="Long weekend">Long weekend</option>
                <option value="Best period">Best period</option>
            </select>
            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
            </div>
        </div>
    </div>

    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
        <div class="flex flex-wrap -mx-3">
            <div class="w-full md:w-1/2 px-3">
                <label for="budget_min">
                    Min Budget
                </label>
                <input id="budget_min" name="budget_min" type="number" min="1" placeholder="Min">
            </div>
            <div class="w-full md:w-1/2 px-3">
                <label for="budget_max">
                    Max Budget
                </label>
                <input
                    class="bg-white leading-tight w-full block appearance-none focus:border-gray-500 focus:outline-none border border-gray-200 py-3 px-4"
                    id="budget_max" name="budget_max" type="number" placeholder="Max">
            </div>
            {{-- <p class="text-sm text-gray-600 italic">Please indicate who much you can spend</p> --}}
        </div>
    </div>

    <div class="w-full md:w-1/3 px-3">
        <label class="block uppercase tracking-wide text-xs font-bold mb-2" for="traveling_with">
            Traveling with
        </label>
        <div class="relative">
            <select
                class="block appearance-none w-full bg-white border border-gray-200 py-3 px-4 pr-8 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="traveling_with" name="traveling_with">
                <option value="alone">Alone</option>
                <option value="partner">Partner</option>
                <option value="family">Family</option>
                <option value="colleagues">Colleagues</option>
                <option value="friends">Friends</option>
                <option value="group">Group</option>
            </select>
            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
            </div>
        </div>
    </div>

</div>