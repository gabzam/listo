<div class="mb-6">
    <label class="block uppercase tracking-wide text-xs font-bold mb-2" for="comments">
        Comments
    </label>
    <textarea
        class="block appearance-none w-full bg-white border border-gray-200 py-3 px-4 pr-8 leading-tight focus:outline-none focus:border-gray-500"
        name="comments" id="comments" cols="30" rows="5"></textarea>
</div>