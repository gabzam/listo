@extends('layouts.app')


@section('content')

<article id="enquiry">

    <div class="flex justify-between -mx-3 pt-16 pb-10">
        <section class="w-full md:w-2/3 px-3 ">
            <div class="shadow p-1 bg-white rounded-lg">
                @include('enquiries.details.map')
            </div>
            <br>
            <div class="border bg-white px-4 pb-5 pt-6 rounded-lg">
                <span class="text-center block mb-3 text-md text-gray-600 capitalize">{{ $enquiry->travel_type }}</span>
                @include('enquiries.details.destination', [ 'number' => 1])

                @if ($enquiry->from_2)
                @include('enquiries.details.destination', [ 'number' => 2])
                @endif

                @if ($enquiry->from_3)
                @include('enquiries.details.destination', [ 'number' => 3])
                @endif

                @if ($enquiry->from_4)
                @include('enquiries.details.destination', [ 'number' => 4])
                @endif

                <span class="text-center block mt-3 text-md text-gray-600">
                    <strong>flexibility:</strong>
                    {{ $enquiry->flexibility }}
                </span>
            </div>
            <br>

            @if ($enquiry->comments)
            <div class="border rounded-lg px-4 pb-5 pt-6 bg-white">
                <h3 class="font-bold">Comments:</h3>
                {{ $enquiry->comments }}
            </div>
            <br>
            @endif

            <div class="border rounded-lg px-4 pb-5 pt-6 bg-white">
                <table class="w-full">
                    <tr>
                        <td><strong>Traveling with:</strong></td>
                        <td>{{ $enquiry->traveling_with }}</td>
                    </tr>
                </table>
            </div>
            <br>

            <div class="border rounded-lg px4 p-5 bg-white">
                @include('enquiries.details.flight')
            </div>

            <br>

            <div class="border rounded-lg px4 p-5 bg-white">
                @include('enquiries.details.accommodation')
                <p class="text-md uppercase text-gray-600 text-center">accommodation</p>
            </div>

            <br>

            <div class="border rounded-lg px4 p-5 bg-white">
                <ul>
                    @foreach ($enquiry->categories as $item)
                    <li>{{ $item->name }}</li>
                    @endforeach
                </ul>
                <p class="text-md uppercase text-gray-600 text-center">Purpose/Theme</p>
            </div>
            <br>
            <div class="border rounded-lg px4 p-5 bg-white">
                <ul>
                    @foreach ($enquiry->countries as $item)
                    <li>{{$item->name}}</li>
                    @endforeach
                </ul>
                <p class="text-md uppercase text-gray-600 text-center">Countries</p>
            </div>
            <br>
            <div class="border rounded-lg px4 p-5 bg-white">
                <ul>
                    @foreach ($enquiry->agencies as $item)
                    <li>{{ $item->name }}</li>
                    @endforeach
                </ul>
                <p class="text-md uppercase text-gray-600 text-center">Assigned Agencies</p>
            </div>


        </section>
        <aside class="w-full md:w-1/3 px-3">
            <div class="rounded-lg border bg-white p-5">
                @include('enquiries.details.user')
            </div>
            <br>
            <div class="text-center rounded-lg border bg-white pb-5 pt-6">
                <h2 class="block tracking-wide text-gray-700 text-large font-bold mb-2">
                    {{ $enquiry->created_at->diffForHumans() }}
                </h2>
                <span class="text-md uppercase text-gray-600">Request date</span>
                <hr class="my-5">
                <p class="block capitalize text-lg">
                    <span class="inline-flex items-center text-gray-700">
                        @include('icons.budget',['style'=>'h-10 w-10 mr-2']) {{ $enquiry->budget_min }} -
                        {{ $enquiry->budget_max }} CHF
                    </span>

                </p>
                <span class="text-md uppercase text-gray-600">Budget</span>
            </div>
        </aside>
    </div>


    <br>

    <br>




    <div class="container px-2 sm:px-6 mt-20 mb-10">


        {{-- {{ $enquiry-> }}

        $table->set('options',['flights','accommodation','transportation','activities'])->nullable()->default('flights');

        $table->string('accommodation_comments')->nullable();
        $table->string('transportation_type')->nullable();
        $table->string('car_type')->nullable();
        $table->string('transportation_comments')->nullable();
        $table->string('themes')->nullable();
        $table->string('activities')->nullable();
        $table->set('contact_mode',['email','phone_call','sms','whatsapp','viber','skype','facebook_messenger'])->nullable();
        --}}

    </div>
</article>
@endsection