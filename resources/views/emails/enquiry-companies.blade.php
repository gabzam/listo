@component('mail::message')
# It's a match!

Great News, Someone has requested an offer that matches your specializations. Check it out

@component('mail::button', ['url' => route('enquiries.show', $enquiry->id)])
Review Request
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent