@component('mail::message')

@if ($enquiry->agencies()->count() > 0)

# Great news! Your enquiry have been sent successfully

Hey {{ $user->firstname }},

We have found {{ $enquiry->agencies()->count() }} agencies in our database matching your request, and we have
automatically sent
them your enquiry on your behalf.
Please give them some time! They will get back to you as soon as possible.

@else

# Your enquiry have been created!

Unfortunatelly, We couldn't find any partner agency in our database specializing in your request.
Please give us some time to find you a good match.

@endif

@component('mail::button', ['url' => route('enquiries.show', $enquiry->id)])
Review enquiry
@endcomponent

Thanks, <br>
{{ config('app.name') }}
@endcomponent