@extends('layouts.auth')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')
<div class="flex justify-center mx-3">
    <div class="my-32 w-full sm:w-1/2 md:w-1/3">
        <div class="flex justify-center">
            @include('icons.logo', ['style'=> 'h-20 w-20'])
        </div>
        <h1 class="my-5 font-bold text-2xl text-center">{{ __('Register') }}</h1>
        <form method="POST" action="{{ route('register') }}" class="kirby-form w-full">
            @csrf

            <div class="mb-6">
                <label for="username">{{ __('Username') }}</label>
                <input id="username" type="text" class="@error('username') field-error @enderror" name="username"
                    value="{{ old('username') }}" autocomplete="username" autofocus>

                @error('username')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label for="firstname">
                        {{ __('Firstname') }}
                    </label>
                    <input class="@error('firstname') field-error @enderror" id="firstname" type="text" name="firstname"
                        placeholder="ex: Jane" value="{{ old('firstname') }}" autocomplete="firstname">
                    @error('firstname')
                    <span role="alert" class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="w-full md:w-1/2 px-3">
                    <label for="grid-last-name">
                        {{ __('Lastname') }}
                    </label>
                    <input class="@error('firstname') field-error @enderror" id="lastname" type="text"
                        placeholder="ex: Doe" value="{{ old('lastname') }}" name="lastname">
                    @error('lastname')
                    <span role="alert" class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="mb-6">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="form-control @error('email') field-error @enderror" name="email"
                    value="{{ old('email') }}" autocomplete="email">

                @error('email')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
            </div>

            <div class="mb-6">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') field-error @enderror"
                        name="password" autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
            </div>

            <div class="mb-6">
                <label for="password-confirm"
                    class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                        autocomplete="new-password">
                </div>
            </div>


            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label for="firstname">
                        {{ __('Birthday') }}
                    </label>
                    <input class="@error('birthday') field-error @enderror" id="birthday" type="text" name="birthday"
                        placeholder="Birthday" value="{{ old('birthday') }}" autocomplete="birthday">
                    @error('birthday')
                    <span role="alert" class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="w-full md:w-1/2 px-3">
                    <label for="grid-last-name">
                        {{ __('Gender') }}
                    </label>
                    <input type="radio" name="gender" id="man" value="male" class="mr-2" required> Man
                    <input type="radio" name="gender" id="woman" value="female" class="ml-4 mr-2"> Woman
                    @error('gender')
                    <span role="alert" class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
            </div>


            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr('#birthday');
</script>
@endpush



{{-- <form class="w-full max-w-lg">


    <div class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full px-3">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                Password
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="grid-password" type="password" placeholder="******************">
            <p class="text-gray-600 text-xs italic">Make it as long and as crazy as you'd like</p>
        </div>
    </div>
    <div class="flex flex-wrap -mx-3 mb-2">
        <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-city">
                City
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="grid-city" type="text" placeholder="Albuquerque">
        </div>
        <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                State
            </label>
            <div class="relative">
                <select
                    class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-state">
                    <option>New Mexico</option>
                    <option>Missouri</option>
                    <option>Texas</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                </div>
            </div>
        </div>
        <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-zip">
                Zip
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="grid-zip" type="text" placeholder="90210">
        </div>
    </div>
</form> --}}