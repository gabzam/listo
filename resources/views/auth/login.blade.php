@extends('layouts.auth')

@section('title','Login')

@section('content')
<div class="flex justify-center mx-3">

    <div class="my-32 w-full sm:w-1/3 md:w-1/4">
        <div class="flex justify-center">
            @include('icons.logo', ['style'=> 'h-20 w-20'])
        </div>

        <h1 class="my-5 font-bold text-2xl text-center">{{ __('Login') }}</h1>

        <form method="POST" action="{{ route('login') }}" class="kirby-form">
            @csrf

            <div class="mb-6">
                <label for="email">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="@error('email') field-error @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="text-red-600 text-sm italic" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="mb-6">
                <label for="password">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="mb-6 inline-flex items-center">
                <input class="inline mr-2" type="checkbox" name="remember" id="remember"
                    {{ old('remember') ? 'checked' : '' }}>

                <label class="mt-3" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>

            <div class="text-center mb-6">
                <button type="submit" class="mx-2">
                    {{ __('Login') }}
                </button>
            </div>

            <div class="text-center my-6">
                <a class="mx-3" href="{{ route('register') }}">{{ __('Register') }}</a> |
                @if (Route::has('password.request'))
                <a class="mx-3" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif
            </div>
        </form>
    </div>
</div>
@endsection