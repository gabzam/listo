@extends('layouts.app')

@php
$title = 'New Country';
$name = '';
$description = '';
$flag = '';
$method = 'POST';
$button = 'Create Country';
$btn_icon = 'icons.add';
@endphp

@isset($country)
@php
$title = 'Editing country: ' . $country->name ;
$name = $country->name;
$description = $country->description;
$flag = $country->flag;
$method = 'PUT';
$button = 'Save changes';
$btn_icon = 'icons.edit';
@endphp
@endisset

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        {{ $title }}
    </h2>
</header>

{{-- @include('shared.form_errors') --}}

<form class="kirby-form"
    action="{{ isset($country) ?  route('countries.update', $country->id) : route('countries.store') }}" method="POST">
    @csrf
    @method($method)
    {{-- Departure & Destination --}}
    <div class="mb-6">
        <label for="name">Name</label>
        <input id="name" type="text" name="name" placeholder="ex: Canada" value="{{ $name }}"
            class="@error('name') field-error @enderror">
        @error('name')
        <p class="italic text-red-600 text-sm">{{ $message }}</p>
        @enderror
    </div>

    <div class="mb-6">
        <label for="flag">Icon</label>
        <input id="flag" type="text" name="flag" value="{{ $flag }}">
    </div>

    <button type="submit">
        @include($btn_icon) {{ $button }}
    </button>
</form>
@endsection