@extends('layouts.app')

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        Countries
    </h2>
</header>


@include('shared.search',['action' => 'countries.index'])


<div class="text-right mb-2">
    <a href="{{route('countries.create')}}" class="kirby-btn">
        @include('icons.add') Add Country
    </a>
</div>

<section id="countries-list" class="mb-16">
    <ul class="data-list">
        @forelse ($countries as $country)
        <li>
            <div class="flex flex-wrap">
                <div class="w-full md:w-2/5">
                    <a href="{{ route('countries.show', $country->id )}}" class="flex justify-between">
                        <div class="inline-flex">
                            <img src="{{$country->flag}}" alt="{{$country->name}}" class="w-10 mr-2">
                            <strong>{{$country->name}}</strong>
                        </div>
                    </a>
                </div>
                <div class="w-full md:w-1/5">
                    {{ $country->region }}
                </div>
                <div class="w-full md:w-1/5 text-left md:text-right">
                    {{ $country->alpha_2_Code }}
                </div>
                <div class="w-full md:w-1/5">
                    @include('icons.next', ['style'=>'h-5 w-5 float-right self-center text-gray-700'])
                </div>
            </div>
        </li>
        @empty

        <li>No Countries found!</li>

        @endforelse
    </ul>

    {{ $countries->appends([ 'search' => request()->query('search') ])->links() }}

</section>
@endsection