<form action="{{ route($action) }}" method="get">
    <input class="w-full border bg-white border-gray-400 py-3 px-4 leading-tight appearance-none block focus:outline-none
    focus:border-gray-600 mb-8 rounded" type="search" name="search" placeholder="Search..."
        value="{{ request()->query('search') }}">
</form>