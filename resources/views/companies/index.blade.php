@extends('layouts.app')

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate text-gray-800">
        Companies
    </h2>
</header>


@include('shared.search',['action' => 'companies.index'])


<div class="text-right mb-2">
    <a href="{{route('companies.create')}}">
        @include('icons.add') Add Agency
    </a>
</div>

<div id="companies-list" class="mb-16">
    <ul class="data-list">
        @forelse ($agencies as $agency)
        <li class="flex flex-wrap">
            <div class="w-full sm:w-2/4 md:w-1/5">
                <a href="{{ route('companies.show', $agency->id )}}"
                    class="flex justify-between inline-flex text-gray-800">
                    <strong>{{$agency->name}}</strong>
                </a>
            </div>
            <div class="w-full sm:w-2/4 md:w-1/5 md:pl-5">
                {{ $agency->country }}
                {{-- <a href="{{ route('agencies.show', $agency->id )}}">
                @include('icons.view') View
                </a> --}}
            </div>
            <div class="w-full sm:w-2/4 md:w-1/5">
                <a href="">
                    @include('icons.phone', ['style'=>'h-4 w-4 mr-1']) {{$agency->phone}}
                </a>
            </div>
            <div class="w-full sm:w-2/4 md:w-1/5">
                <a href="">
                    @include('icons.email') {{$agency->email}}
                </a>
            </div>
            <div class="w-full sm:w-full md:w-1/5">
                <a href="" class="float-right">
                    @include('icons.next', ['style'=>'h-5 w-5 self-center text-gray-700 hover:text-pink-800'])
                </a>

            </div>
        </li>
        @empty

        <li>No Companies found!</li>

        @endforelse
    </ul>

    {{ $agencies->appends([ 'search' => request()->query('search') ])->links() }}

    @if ($trashed_button)
    @auth
    <div class="text-center my-10">
        <a href="{{ route('trashed_agencies.trashed') }}" class="kirby-btn my-2">
            @include('icons.trash')
            View trashed
        </a>
    </div>
    @endauth
    @endif

</div>
@endsection