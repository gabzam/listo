@extends('layouts.app')

@section('content')
<section class="flex justify-center">
    <div class="w-full md:w-2/3">
        <header class="py-8">
            <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate text-gray-800">
                Editing User: {{ $user->firstname }} {{ $user->lastname }}
            </h2>
        </header>

        <form class="kirby-form" action="{{ route('users.update', $user->id) }}" method="POST"
            enctype="multipart/form-data">
            @csrf
            @method('PUT')

            @include('users.forms.user')
            @include('users.forms.personal')

            <br>

            <h3 class="font-bold text-gray-800 text-lg mb-3">Contact info</h3>
            @include('users.forms.contact')
            <h3 class="font-bold text-gray-800 text-lg mb-3">Address</h3>
            @include('users.forms.address')

            <br>

            <h3 class="font-bold text-gray-800 text-lg mb-3">Passport(s)</h3>
            @include('users.forms.passport')

            <h3 class="font-bold text-gray-800 text-lg mb-3">Role(s)</h3>
            @include('users.forms.roles')

            <button type="submit">
                Edit User
            </button>
        </form>
    </div>
</section>
@endsection