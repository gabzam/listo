<a href="{{ $user->facebook }}" class="{{ isset($user->facebook) ? 'mx-1' : 'hidden'}}">
    @include('icons.social.facebook', ['style'=>'h-6 w-6'])
</a>
<a href="{{ $user->instagram }}" class="{{ isset($user->instagram) ? 'mx-1' : 'hidden'}}">
    @include('icons.social.instagram', ['style'=>'h-6 w-6'])
</a>
<a href="{{ $user->twitter }}" class="{{ isset($user->twitter) ? 'mx-1' : 'hidden'}}">
    @include('icons.social.twitter', ['style'=>'h-6 w-6'])
</a>
<a href="{{ $user->snapchat }}" class="{{ isset($user->snapchat) ? 'mx-1' : 'hidden'}}">
    @include('icons.social.snapchat', ['style'=>'h-6 w-6'])
</a>
<a href="{{ $user->youtube }}" class="{{ isset($user->youtube) ? 'mx-1' : 'hidden'}}">
    @include('icons.social.youtube', ['style'=>'h-6 w-6'])
</a>
<a href="{{ $user->pinterest }}" class="{{ isset($user->pinterest) ? 'mx-1' : 'hidden'}}">
    @include('icons.social.pinterest', ['style'=>'h-6 w-6'])
</a>
<a href="{{ $user->skype }}" class="{{ isset($user->skype) ? 'mx-1' : 'hidden'}}">
    @include('icons.social.skype', ['style'=>'h-6 w-6'])
</a>
<a href="{{ $user->linkedin }}" class="{{ isset($user->linkedin) ? 'mx-1' : 'hidden'}}">
    @include('icons.social.linkedin', ['style'=>'h-6 w-6'])
</a>
{{-- <a href="{{ $user->tiktok }}" class="{{ isset($user->tiktok) ? 'mx-1' : 'hidden'}}">
@include('icons.social.tiktok', ['style'=>'h-6 w-6'])
tiktok
</a> --}}