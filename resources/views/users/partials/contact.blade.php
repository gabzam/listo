<table>
    <tr>
        @isset($user->email)
        <td class="px-2">
            <a href="mailto:{{ $user->email }}" class="inline-flex hover:text-pink-800 hover:underline">
                @include('icons.email-circled', ['style'=>'h-6 w-6 mr-2'])
                {{ $user->email }}
            </a>
            @isset($user->mobile)
            <a href="tel:{{ $user->mobile }}" class="sm:hidden inline-flex hover:text-pink-800 hover:underline block">
                @include('icons.phone-circled', ['style'=>'h-6 w-6 mr-2'])
                {{ $user->mobile }}
            </a>
            @endisset
            @isset($user->phone)
            <a href="tel:{{ $user->phone }}" target="_blank"
                class="sm:hidden inline-flex hover:text-pink-800 hover:underline block">
                @include('icons.phone-circled', ['style'=>'h-6 w-6 mr-2'])
                {{ $user->phone }}
            </a>
            @endisset
        </td>
        @endisset
        @isset($user->mobile)
        <td class="hidden sm:table-cell px-2">
            <a href="tel:{{ $user->mobile }}" class="inline-flex hover:text-pink-800 hover:underline">
                @include('icons.phone-circled', ['style'=>'h-6 w-6 mr-2'])
                {{ $user->mobile }}
            </a>
        </td>
        @endisset
        @isset($user->phone)
        <td class="hidden sm:table-cell px-2">
            <a href="{{ $user->phone }}" target="_blank" class="inline-flex hover:text-pink-800 hover:underline">
                @include('icons.phone-circled', ['style'=>'h-6 w-6 mr-2'])
                {{ $user->phone }}
            </a>
        </td>
        @endisset
    </tr>
</table>