<h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate text-gray-800">
    {{ $user->firstname }} {{ $user->lastname }}
</h2>
<span class="text-center text-gray-600">{{ $user->username }}</span>
<div class="flex justify-center mt-2 mb-6 text-gray-700">
    <div class="max-w-6xl">
        @isset($user->gender)
        <span class="mx-3 inline-flex">
            @if ($user->gender === 'male')
            @include('icons.male-sign')
            @else
            @include('icons.female-sign')
            @endif
            {{ $user->gender }}
        </span>
        @endisset
        @if ($user->birthday)
        <span class="mx-3 inline-flex">@include('icons.birthday')
            {{ $user->birthday->format('M-d-Y') }}</span>
        @endif
        @if ($user->profession)
        <span class="mx-3 inline-flex">@include('icons.worker') {{ $user->profession }}</span>
        @endif
    </div>
</div>

<div class="text-center mt-10">
    <img src="{{ asset($user->avatar) }}" class="w-1/2 sm:w-1/3 md:w-1/5 block mx-auto rounded-full"
        alt="{{ $user->firstname }}">
</div>