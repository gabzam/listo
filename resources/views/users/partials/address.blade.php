<table>
    <tr>
        <td class="inline-flex sm:pr-5">@include('icons.home')<strong class="hidden sm:inline">Address:</strong>
        </td>
        <td>{{ $user->street }} {{ $user->street_number }}</td>
    </tr>
    @if ($user->address_supplement)
    <tr>
        <td></td>
        <td>{{ $user->address_supplement }}</td>
    </tr>
    @endif
    <tr>
        <td></td>
        <td>
            @if ($user->postal_code)
            {{ $user->postal_code }},
            @endif
            {{ $user->location }}</td>
    </tr>
    <tr>
        <td></td>
        <td>
            @if ($user->state)
            {{ $user->state }},
            @endif
            {{ $user->country }}</td>
    </tr>
    @if ($user->lat)
    <tr>
        <td></td>
        <td>@include('icons.pinpoint', ['style'=>'inline h-5 w-5 align-top']) {{ $user->lat }}, {{ $user->lng }}
        </td>
    </tr>
    @endif
</table>