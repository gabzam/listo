<div class="w-1/3">
    <h3 class="text-center my-3 font-bold text-lg text-gray-800">Passport(s)</h3>
    <table class="w-full">
        <thead>
            <tr>
                {{-- <th>Flag</th> --}}
                <th class="text-left text-gray-800">Passport Country</th>
                <th class="text-left text-gray-800">Expiry date</th>
            </tr>
        </thead>
        <tbody>
            @isset($user->passport1)
            <tr>
                {{-- <td></td> --}}
                <td>{{ $user->passport1 }}</td>
                <td>{{ $user->passport1_expiry }}</td>
            </tr>
            @endisset

            @isset($user->passport2)
            <tr>
                {{-- <td></td> --}}
                <td>{{$user->passport2}}</td>
                <td>{{ $user->passport2_expiry }}</td>
            </tr>
            @endisset

            @isset($user->passport3)
            <tr>
                {{-- <td></td> --}}
                <td>{{ $user->passport3 }}</td>
                <td>{{ $user->passport3_expiry }}</td>
            </tr>
            @endisset

            @isset($user->passport4)
            <tr>
                {{-- <td></td> --}}
                <td>{{ $user->passport4 }}</td>
                <td>{{ $user->passport4_expiry }}</td>
            </tr>
            @endisset
        </tbody>
    </table>
</div>