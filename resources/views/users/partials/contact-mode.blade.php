<table class="w-full">
    @if ($user->tiktok)
    <tr>
        <td><strong>tiktok:</strong></td>
        <td>{{$user->tiktok}}</td>
    </tr>
    @endif

    <tr>
        <td><strong>Whatsapp:</strong></td>
        <td>{{ $user->whatsapp ? 'Yes' : 'No'}}</td>
    </tr>


    <tr>
        <td><strong>Viber:</strong></td>
        <td>{{ $user->viber ? 'Yes' : 'No'}}</td>
    </tr>

    <tr>
        <td><strong>WeChat:</strong></td>
        <td>{{ $user->wechat ? 'Yes' : 'No'}}</td>
    </tr>

    <tr>
        <td><strong>Telegram:</strong></td>
        <td>{{ $user->telegram ? 'Yes' : 'No'}}</td>
    </tr>

    <tr>
        <td><strong>Phone call:</strong></td>
        <td>{{ $user->call ? 'Yes' : 'No'}}</td>
    </tr>

    <tr>
        <td><strong>SMS:</strong></td>
        <td>{{ $user->sms ? 'Yes' : 'No'}}</td>
    </tr>
</table>