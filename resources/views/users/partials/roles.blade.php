@foreach ($user->roles as $r)
<span class="block px-2 rounded-full text-gray-800 bg-gray-200 mx-2">{{ $r->name }}</span>
@endforeach

{{-- @foreach ($user->permissions() as $item)
{{ $item }}
@endforeach --}}