@extends('layouts.app')

@section('content')
<header class="py-8 text-center">
    @include('users.partials.user')
</header>

<div class="flex justify-center mb-10">
    @include('users.partials.social')
</div>

@if ($user->biography)
<div class="flex justify-center">
    <p class="text-center mx-3 w-full md:w-1/2">{{ $user->biography }}</p>
</div>
@endif


@isset($user->street)
<section id="user-address" class="my-10 flex justify-center">
    @include('users.partials.address')
</section>
@endisset

<div class="flex justify-center my-10">
    @include('users.partials.contact')
</div>

<div class="flex justify-center mb-10">
    <div class="w-1/3">
        <h3 class="text-center my-3 font-bold text-lg text-gray-800">Contact mode</h3>
        @include('users.partials.contact-mode')
    </div>
</div>

@isset($user->passport1)
<div class="flex justify-center mb-10">
    @include('users.partials.passports')
</div>
@endisset

@isset($user->roles)
<div class="mb-10">
    <h3 class="text-center my-3 font-bold text-lg text-gray-800">Role(s)</h3>
    <div class="flex justify-center">
        @include('users.partials.roles')
    </div>
</div>
@endisset

<div class="my-20 flex justify-center">
    <a href="{{ route('users.edit', $user->id) }}"
        class="bg-black text-white px-3 py-2 hover:text-white hover:bg-pink-800 rounded">
        @include('icons.edit')
        Edit User
    </a>
    @include('shared.delete', ['model'=> $user, 'action'=>'users.destroy'])
</div>

@endsection