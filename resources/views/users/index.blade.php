{{-- tutotial: https://codepen.io/brbcoding-the-selector/pen/dyPeMOX --}}
@extends('layouts.app')

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        Users
    </h2>
</header>

@livewire('users-list')


{{-- 

    @include('shared.search',['action' => 'users.index'])
<div class="text-right mb-2">
    <a href="{{route('users.create')}}"
class="bg-gray-200 hover:bg-pink-800 hover:text-gray-200 font-bold py-2 px-4 rounded">
@include('icons.add') Add User
</a>
</div>

<section id="users-list" class="mb-16">
    <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
        <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
            <table class="min-w-full leading-normal" x-data="{ 'isDialogOpen': false }"
                @keydown.escape="isDialogOpen = false">
                <thead>
                    <tr>
                        <th
                            class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            User</th>
                        <th
                            class="hidden md:table-cell px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            Email</th>
                        <th
                            class="hidden md:table-cell px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            Phone</th>
                        <th
                            class="hidden md:table-cell px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            Country</th>
                        <th
                            class="hidden md:table-cell px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            Role</th>
                        <th
                            class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $user)
                    <tr>
                        <td class="px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            <a href="{{ route('users.show', $user->id )}}" class="flex justify-between">
                                <div class="inline-flex items-center">
                                    <img src="{{$user->avatar}}" alt="{{$user->name}}"
                                        class="w-10 mr-2 rounded-full border border-white">
                                    <strong>{{$user->firstname}} {{$user->lastname}}</strong>
                                </div>
                            </a>
                            <span class="sm:hidden block">{{ $user->email }}</span>
                            <span class="sm:hidden block">{{ $user->phone }}</span>
                            <span class="sm:hidden block">{{ $user->passport1 }}</span>
                            <span class="sm:hidden block">admin</span>

                        </td>
                        <td class="hidden md:table-cell px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            {{ $user->email }}
                        </td>
                        <td class="hidden md:table-cell px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            {{ $user->phone }}
                        </td>
                        <td class="hidden md:table-cell px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            {{ $user->passport1 }}
                        </td>
                        <td class="hidden md:table-cell px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            admin
                        </td>
                        <td class="px-5 py-2 border-b border-gray-200 bg-white text-sm"
                            x-data="{ 'isHamburgerOpen': false }">
                            <button type="button" title="Options" class="text-right" @click="isHamburgerOpen = true"
                                :class="{ 'bg-gray-100': isHamburgerOpen }">
                                @include('icons.3-dots-h', ['style'=>'h-5 w-5 float-right self-center text-gray-700'])
                            </button>

                            <ul x-show="isHamburgerOpen" x-cloak @click.away="isHamburgerOpen = false"
                                class="absolute border bg-white shadow-md text-left -mt-10 -ml-12 rounded">
                                <li class="py-2 px-3 hover:bg-gray-200">
                                    <a href="{{ route('users.show', $user->id )}}">
                                        @include('icons.view') View
                                    </a>
                                </li>
                                <li class="p-2 hover:bg-gray-200">
                                    @include('icons.x', ['style'=>'h-5 w-5 float-right self-center text-gray-700'])
                                    Delete
                                </li>
                                <li class="p-2 hover:bg-gray-200">🦄 Other action</li>
                            </ul>


                        </td>
                    </tr>

                    @empty

                    <tr>
                        <td>No Users found!</td>
                    </tr>

                    @endforelse

                </tbody>

            </table>
        </div>
    </div>
    {{ $users->appends([ 'search' => request()->query('search') ])->links() }} --}}

</section>
@endsection