<div class="w-full md:w-1/2 mb-6">
    @foreach ($roles as $r)
    <div>
        <input type="checkbox" name="roles[]" id="roles" class="mr-2 leading-tight" value="{{ $r->id }}"
            {{ $user->hasRole($r->id) ? 'checked' : ''}}>
        <span class="text-sm">
            {{ $r->name }}
        </span>
    </div>
    @endforeach
</div>