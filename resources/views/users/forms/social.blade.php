<div class="mb-6">
    <label for="facebook" class="col-md-4 col-form-label text-md-right">{{ __('Facebook') }}</label>
    <input id="facebook" type="text" class="form-control @error('facebook') field-error @enderror" name="facebook"
        value="{{ old('facebook') }}">

    @error('facebook')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>

<div class="mb-6">
    <label for="linkedin" class="col-md-4 col-form-label text-md-right">{{ __('Linkedin') }}</label>
    <input id="linkedin" type="text" class="form-control @error('linkedin') field-error @enderror" name="linkedin"
        value="{{ old('linkedin') }}">

    @error('linkedin')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>

<div class="mb-6">
    <label for="instagram" class="col-md-4 col-form-label text-md-right">{{ __('Instagram') }}</label>
    <input id="instagram" type="text" class="form-control @error('instagram') field-error @enderror" name="instagram"
        value="{{ old('instagram') }}">

    @error('instagram')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>

<div class="mb-6">
    <label for="youtube" class="col-md-4 col-form-label text-md-right">{{ __('Youtube') }}</label>
    <input id="youtube" type="text" class="form-control @error('youtube') field-error @enderror" name="youtube"
        value="{{ old('youtube') }}">

    @error('youtube')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>

<div class="mb-6">
    <label for="tiktok" class="col-md-4 col-form-label text-md-right">{{ __('Tiktok') }}</label>
    <input id="tiktok" type="text" class="form-control @error('tiktok') field-error @enderror" name="tiktok"
        value="{{ old('tiktok') }}">

    @error('tiktok')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>

<div class="mb-6">
    <label for="twitter" class="col-md-4 col-form-label text-md-right">{{ __('Twitter') }}</label>
    <input id="twitter" type="text" class="form-control @error('twitter') field-error @enderror" name="tiktok"
        value="{{ old('twitter') }}">

    @error('twitter')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>

<div class="mb-6">
    <label for="pinterest" class="col-md-4 col-form-label text-md-right">{{ __('Pinterest') }}</label>
    <input id="pinterest" type="text" class="form-control @error('pinterest') field-error @enderror" name="pinterest"
        value="{{ old('pinterest') }}">

    @error('pinterest')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>

<div class="mb-6">
    <label for="skype" class="col-md-4 col-form-label text-md-right">{{ __('skype') }}</label>
    <input id="skype" type="text" class="form-control @error('skype') field-error @enderror" name="skype"
        value="{{ old('skype') }}">

    @error('skype')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>

<div class="mb-6">
    <label for="snapchat" class="col-md-4 col-form-label text-md-right">{{ __('Snapchat') }}</label>
    <input id="snapchat" type="text" class="form-control @error('snapchat') field-error @enderror" name="snapchat"
        value="{{ old('snapchat') }}">

    @error('snapchat')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>