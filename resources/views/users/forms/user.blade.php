<div class="flex flex-wrap -mx-3 mb-6">
    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
        <label for="firstname">
            {{ __('Firstname') }}
        </label>
        <input class="@error('firstname') field-error @enderror" id="firstname" type="text" name="firstname"
            placeholder="ex: Jane" value="{{ $user->firstname ?? old('firstname') }}" autocomplete="firstname"
            autofocus>
        @error('firstname')
        <span role="alert" class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>

    <div class="w-full md:w-1/2 px-3">
        <label for="grid-last-name">
            {{ __('Lastname') }}
        </label>
        <input class="@error('firstname') field-error @enderror" id="lastname" type="text" placeholder="ex: Doe"
            value="{{ $user->lastname ?? old('lastname') }}" name="lastname">

        @error('lastname')
        <span role="alert" class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>

    <div class="w-full md:w-1/3 px-3">
    </div>

</div>

<div class="flex flex-wrap -mx-3 mb-6">

    <div class="w-full md:w-1/2 px-3">
        <label for="email">{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="form-control @error('email') field-error @enderror" name="email"
            value="{{ $user->email ?? old('email') }}">

        @error('email')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
        @enderror
    </div>

    <div class="w-full md:w-1/2 px-3">
        <label for="username">{{ __('Username') }}</label>
        <input id="username" type="text" class="@error('username') field-error @enderror" name="username"
            value="{{ $user->username ?? old('username') }}" autocomplete="username">
        @error('username')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
        @enderror
    </div>
</div>


{{-- <div class="w-full md:w-1/3 px-3">
    <label for="password">{{ __('Password') }}</label>
<input id="password" name="password" type="password" class="form-control @error('password') field-error @enderror"
    value="{{ old('password') }}" autocomplete="password">

@error('password')
<span class="invalid-feedback" role="alert">
    {{ $message }}
</span>
@enderror
</div> --}}