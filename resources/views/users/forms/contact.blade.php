<div class="flex flex-wrap -mx-3 mb-6">
    <div class="w-full md:w-1/2 px-3">
        <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>
        <input id="mobile" type="text" class="form-control @error('mobile') field-error @enderror" name="mobile"
            value="{{ $user->mobile ?? old('mobile') }}" autocomplete="mobile">

        @error('mobile')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
        @enderror
    </div>
    <div class="w-full md:w-1/2 px-3">
        <label for="phone">{{ __('Phone / Mobile 2') }}</label>
        <input id="phone" name="phone" type="text" class="form-control @error('phone') field-error @enderror"
            value="{{ $user->phone ?? old('phone') }}" autocomplete="mobile">

        @error('phone')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
        @enderror
    </div>
</div>


<div class="mb-6">
    <label for="phone">{{ __('Contact mode') }}</label>

    <div class="inline-flex md:mr-3">
        @if ($user ?? '')
        <input id="call" name="call" type="checkbox" value="1"
            {{ $user->call || old('call') ? 'checked="checked"' : '' }}>
        @else
        <input id="call" name="call" type="checkbox" value="1" {{ old('call') ? 'checked="checked"' : '' }}>
        @endif
        <label for="call" class="pl-2">{{ __('Phone Call') }}</label>
    </div>


    <div class="inline-flex md:mr-3">
        @if ($user ?? '')
        <input id="whatsapp" name="whatsapp" type="checkbox" value="1"
            {{ $user->whatsapp || old('whatsapp') ? 'checked="checked"' : '' }}>
        @else
        <input id="whatsapp" name="whatsapp" type="checkbox" value="1" {{ old('whatsapp') ? 'checked="checked"' : '' }}>
        @endif
        <label for="whatsapp" class="pl-1">{{ __('Whatsapp') }}</label>
    </div>

    <div class="inline-flex md:mr-3">
        @if ($user ?? '')
        <input id="viber" name="viber" type="checkbox" value="1"
            {{ $user->viber || old('viber') ? 'checked="checked"' : '' }}>
        @else
        <input id="viber" name="viber" type="checkbox" value="1" {{ old('viber') ? 'checked="checked"' : '' }}>
        @endif
        <label for="viber" class="pl-1">{{ __('Viber') }}</label>
    </div>

    <div class="inline-flex md:mr-3">
        @if ($user ?? '')
        <input id="wechat" name="wechat" type="checkbox" value="1"
            {{ $user->wechat || old('wechat') ? 'checked="checked"' : '' }}>
        @else
        <input id="wechat" name="wechat" type="checkbox" value="1" {{ old('wechat') ? 'checked="checked"' : '' }}>
        @endif
        <label for="wechat" class="pl-1">{{ __('Wechat') }}</label>
    </div>

    <div class="inline-flex md:mr-3">
        @if ($user ?? '')
        <input id="telegram" name="telegram" type="checkbox" value="1"
            {{ $user->telegram || old('telegram') ? 'checked="checked"' : '' }}>
        @else
        <input id="telegram" name="telegram" type="checkbox" value="1" {{ old('telegram') ? 'checked="checked"' : '' }}>
        @endif
        <label for="telegram" class="pl-1">{{ __('Telegram') }}</label>
    </div>

    <div class="inline-flex">
        @if ($user ?? '')
        <input id="sms" name="sms" type="checkbox" value="1" {{ $user->sms || old('sms') ? 'checked="checked"' : '' }}>
        @else
        <input id="sms" name="sms" type="checkbox" value="1" {{ old('sms') ? 'checked="checked"' : '' }}>
        @endif
        <label for="sms" class="pl-2">{{ __('SMS') }}</label>
    </div>
</div>
<br>