<div class="flex flex-wrap -mx-3 mb-6">

    <div class="w-full md:w-1/3 px-3">
        <label for="birthday">{{ __('Birthday') }}</label>
        <input id="birthday" type="date" class="form-control @error('birthday') field-error @enderror" name="birthday"
            value="{{ $user->birthday ?? old('birthday') }}">

        @error('birthday')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
        @enderror
    </div>

    <div class="w-full md:w-1/3 px-3">
        <label for="gender">{{ __('gender') }}</label>
        <select name="gender" id="gender">
            @if($user ?? '')
            <option value="male" @if($user->gender === 'male') selected @endif }}>Male</option>
            <option value="female" @if($user->gender === 'female') selected @endif>Female</option>
            @else
            <option disabled selected value>Select gender</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
            @endif
        </select>

        @error('gender')
        <span class="invalid-feedback" role="alert">
            {{ $message }}
        </span>
        @enderror
    </div>

    <div class="w-full md:w-1/3 px-3">
        <label for="picture">{{ __('Picture') }}</label>
        <input type="file" name="picture" id="picture" value="{{ $user->picture ?? old('value') }}">
    </div>
</div>

<div class="mb-6">
    <label for="biography" class="col-md-4 col-form-label text-md-right">{{ __('Biography') }}</label>
    <textarea name="biography" id="biography" cols="30" rows="5"
        class="form-control @error('biography') field-error @enderror">{{ $user->biography ?? old('biography') }}</textarea>

    @error('biography')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>


<div class="mb-6">
    <label for="profession" class="col-md-4 col-form-label text-md-right">{{ __('Profession') }}</label>
    <input id="profession" type="text" class="form-control @error('address') field-error @enderror" name="profession"
        value="{{ $user->profession ?? old('profession') }}">

    @error('profession')
    <span class="invalid-feedback" role="alert">
        {{ $message }}
    </span>
    @enderror
</div>