<div x-data="{ pass2: false, pass3: false, pass4: false }" class="my-6">
    <div class="flex flex-wrap -mx-4 mb-6">
        <div class="w-full md:w-2/4 px-3">
            <label for="passport1">{{ __('Passport') }}</label>
            <input id="passport1" name="passport1" type="text" class="form-control" list="countries"
                value="{{ $user->passport1 ?? old('passport1')}}">
        </div>
        <div class="w-full md:w-1/4 px-3">
            <label for="passport1_expiry">{{ __('Expiry date') }}</label>
            <input id="passport1_expiry" name="passport1_expiry" type="date" class="form-control"
                value="{{ $user->passport1_expiry ?? old('passport1_expiry')}}">
        </div>
        <div class="w-full md:w-1/4 px-3">
            <br>
            <button @click="pass2 = true" type="button" class="mt-3 hover:text-pink-800">@include('icons.add')</button>
        </div>
    </div>

    <div x-show="pass2" class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full md:w-2/4 px-3">
            <label for="passport2">{{ __('Passport 2') }}</label>
            <input id="passport2" name="passport2" type="text" class="form-control" list="countries"
                value="{{ $user->passport2 ?? old('passport2')}}">
        </div>
        <div class="w-full md:w-1/4 px-3">
            <label for="passport2_expiry">{{ __('Expiry date') }}</label>
            <input id="passport2_expiry" name="passport2_expiry" type="date" class="form-control"
                value="{{ $user->passport2_expiry ?? old('passport2_expiry')}}">
        </div>
        <div class="w-full md:w-1/4 px-3">
            <br>
            <button @click="pass3 = true" type="button" class="mt-3 hover:text-pink-800">@include('icons.add')</button>
            <button @click="pass2 = false" type="button"
                class="mt-3 hover:text-pink-800">@include('icons.minus-circle')</button>
        </div>
    </div>

    <div x-show="pass3" class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full md:w-2/4 px-3">
            <label for="passport3">{{ __('Passport 3') }}</label>
            <input id="passport3" name="passport3" type="text" class="form-control" list="countries"
                value="{{ $user->passport3 ?? old('passport3')}}">
        </div>
        <div class="w-full md:w-1/4 px-3">
            <label for="passport3_expiry">{{ __('Expiry date') }}</label>
            <input id="passport3_expiry" name="passport3_expiry" type="date" class="form-control"
                value="{{ $user->passport3_expiry ?? old('passport3_expiry')}}">
        </div>
        <div class="w-full md:w-1/4 px-3">
            <br>
            <button @click="pass4 = true" type="button" class="mt-3 hover:text-pink-800">@include('icons.add')</button>
            <button @click="pass3 = false" type="button"
                class="mt-3 hover:text-pink-800">@include('icons.minus-circle')</button>
        </div>
    </div>

    <div x-show="pass4" class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full md:w-2/4 px-3">
            <label for="passport4">{{ __('Passport 4') }}</label>
            <input id="passport4" name="passport4" type="text" class="form-control" list="countries"
                value="{{ $user->passport4 ?? old('passport4')}}">
        </div>
        <div class="w-full md:w-1/4 px-3">
            <label for="passport4_expiry">{{ __('Expiry date') }}</label>
            <input id="passport4_expiry" name="passport4_expiry" type="date" class="form-control"
                value="{{ $user->passport4_expiry ?? old('passport4_expiry')}}">
        </div>
        <div class="w-full md:w-1/4 px-3">
            <br>
            <button @click="pass4 = false" type="button"
                class="mt-3 hover:text-pink-800">@include('icons.minus-circle')</button>
        </div>
    </div>
</div>


<datalist id="countries">
    @foreach ($countries as $c)
    <option value="{{ $c->name }}">
        @endforeach
</datalist>