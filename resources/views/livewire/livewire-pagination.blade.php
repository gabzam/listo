{{-- tutotail https://tailwindcomponents.com/component/bootstrap-style-pagination --}}

@if ($paginator->hasPages())
<ul class="flex pl-0 list-none rounded" role="navigation">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
    <li class="relative block py-2 px-3 leading-tight bg-white border border-gray-300 text-gray-500 border-r-0 ml-0 rounded-l-md disabled"
        aria-disabled="true" aria-label="@lang('pagination.previous')">
        <span class="inline-flex items-center" aria-hidden="true">
            @include('icons.chevron-left', ['style' => 'h-3 w-3 mr-2'])
            Previous
            {{-- @lang('pagination.previous') --}}
        </span>
    </li>
    @else
    <li
        class="relative block py-2 px-3 leading-tight bg-white border border-gray-300 text-gray-700 border-r-0 hover:bg-gray-200 ml-0 rounded-l-md">
        <button type="button" class="page-link focus:outline-none" wire:click="previousPage" rel="prev"
            aria-label="@lang('pagination.previous')">
            <span class="inline-flex items-center">
                @include('icons.chevron-left', ['style' => 'h-3 w-3 mr-2'])
                Previous
                {{-- @lang('pagination.previous') --}}
            </span>
        </button>
    </li>
    @endif

    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
    {{-- "Three Dots" Separator --}}
    @if (is_string($element))
    <li class="relative block py-2 px-3 leading-tight bg-white border border-gray-300 text-gray-700 border-r-0 hover:bg-gray-200 disabled d-none d-md-block"
        aria-disabled="true"><span class="page-link">{{ $element }}</span>
    </li>
    @endif

    {{-- Array Of Links --}}
    @if (is_array($element))
    @foreach ($element as $page => $url)
    @if ($page == $paginator->currentPage())
    <li class="relative block py-2 px-3 leading-tight bg-white border border-gray-300 text-gray-700 border-r-0 hover:bg-gray-200 active bg-gray-100"
        aria-current="page"><span class="page-link">{{ $page }}</span></li>
    @else
    <li
        class="relative block py-2 px-3 leading-tight bg-white border border-gray-300 text-gray-700 border-r-0 hover:bg-gray-200">
        <button type="button" class="page-link" wire:click="gotoPage({{ $page }})">{{ $page }}</button></li>
    @endif
    @endforeach
    @endif
    @endforeach

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <li
        class="relative block py-2 px-3 leading-tight bg-white border border-gray-300 text-gray-700 rounded-r hover:bg-gray-200">
        <button type="button" class="page-link focus:outline-none" wire:click="nextPage" rel="next"
            aria-label="@lang('pagination.next')">
            <span class="inline-flex items-center">
                Next
                @include('icons.chevron-right', ['style' => 'h-3 w-3 ml-2'])
            </span>
        </button>
    </li>
    @else
    <li class="relative block py-2 px-3 leading-tight bg-white border border-gray-300 text-gray-500 rounded-r disabled"
        aria-disabled="true" aria-label="@lang('pagination.next')">
        <span class="inline-flex items-center">
            Next
            @include('icons.chevron-right', ['style' => 'h-3 w-3 ml-2'])
        </span>
    </li>
    @endif
</ul>
@endif