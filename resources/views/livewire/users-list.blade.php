<section id="users-list" class="mb-16">

    <div class="flex flex-wrap">
        <div class="w-full md:w-2/3">
            <div class="inline-block relative">
                <select wire:model="perPage"
                    class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded leading-tight focus:outline-none">
                    <option>10</option>
                    <option>15</option>
                    <option>25</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                </div>
            </div>

            <div class="inline-block w-2/3">
                <div class="block relative">
                    <span class="h-full absolute inset-y-0 left-0 flex items-center pl-2">
                        <svg viewBox="0 0 24 24" class="h-4 w-4 fill-current text-gray-500">
                            <path
                                d="M10 4a6 6 0 100 12 6 6 0 000-12zm-8 6a8 8 0 1114.32 4.906l5.387 5.387a1 1 0 01-1.414 1.414l-5.387-5.387A8 8 0 012 10z">
                            </path>
                        </svg>
                    </span>
                    <input wire:model="search" type="search" placeholder="Search..." class="pl-8 pr-6 w-full border bg-white border-gray-400 py-2 px-4 leading-tight appearance-none block focus:outline-none
                    focus:border-gray-600 mb-3 rounded text-gray-800">
                </div>
            </div>

        </div>
        <div class="w-full md:w-1/3">
            <div class="text-right mb-2">
                <a href="{{route('users.create')}}"
                    class="bg-gray-200 hover:bg-pink-800 hover:text-gray-200 font-bold py-2 px-4 rounded">
                    @include('icons.add') Add User
                </a>
            </div>
        </div>
    </div>

    <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-2 overflow-x-auto">
        <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
            <table class="min-w-full leading-normal" x-data="{ 'isDialogOpen': false }"
                @keydown.escape="isDialogOpen = false">
                <thead>
                    <tr>
                        <th
                            class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            <a href="#" wire:click.prevent="sortBy('firstname')" role="button"
                                class="flex justify-between">
                                User
                                @include('shared.sort_icons', ['field'=>'firstname'])
                            </a>
                        </th>
                        <th
                            class="hidden md:table-cell px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            <a href="#" wire:click.prevent="sortBy('email')" role="button" class="flex justify-between">
                                Email
                                @include('shared.sort_icons', ['field'=>'email'])
                            </a>
                        </th>
                        <th
                            class="hidden md:table-cell px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            <a href="#" wire:click.prevent="sortBy('phone')" role="button" class="flex justify-between">
                                Phone
                                @include('shared.sort_icons', ['field'=>'phone'])
                            </a>
                        </th>
                        <th
                            class="hidden md:table-cell px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            <a href="#" wire:click.prevent="sortBy('country')" role="button"
                                class="flex justify-between">
                                Country
                                @include('shared.sort_icons', ['field'=>'country'])
                            </a>
                        </th>
                        <th
                            class="hidden md:table-cell px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                            Role</th>
                        <th
                            class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-900 uppercase tracking-wider">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $user)
                    <tr>
                        <td class="px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            <a href="{{ route('users.show', $user->id )}}" class="flex justify-between">
                                <div class="inline-flex items-center">
                                    <img src="{{$user->avatar}} " alt="{{$user->firstname}}"
                                        class="w-10 mr-2 rounded-full border border-white">
                                    <strong>{{$user->firstname}} {{$user->lastname}}</strong>
                                </div>
                            </a>
                            <span class="sm:hidden block">{{ $user->email }}</span>
                            <span class="sm:hidden block">{{ $user->phone }}</span>
                            <span class="sm:hidden block">{{ $user->country }}</span>
                            <span class="sm:hidden block">admin</span>

                        </td>
                        <td class="hidden md:table-cell px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            {{ $user->email }}
                        </td>
                        <td class="hidden md:table-cell px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            {{ $user->phone }}
                        </td>
                        <td class="hidden md:table-cell px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            {{ $user->country }}
                        </td>
                        <td class="hidden md:table-cell px-5 py-2 border-b border-gray-200 bg-white text-sm">
                            admin
                        </td>
                        <td class="px-5 py-2 border-b border-gray-200 bg-white text-sm"
                            x-data="{ 'isHamburgerOpen': false }">
                            <button type="button" title="Options" class="text-right" @click="isHamburgerOpen = true"
                                :class="{ 'bg-gray-100': isHamburgerOpen }">
                                @include('icons.3-dots-h', ['style'=>'h-5 w-5 float-right self-center
                                text-gray-700'])
                            </button>

                            <ul x-show="isHamburgerOpen" x-cloak @click.away="isHamburgerOpen = false"
                                class="absolute border bg-white shadow-md text-left -mt-10 -ml-12 rounded">
                                <li class="py-2 px-3 hover:bg-gray-200">
                                    <a href="{{ route('users.show', $user->id )}}">
                                        @include('icons.view') View
                                    </a>
                                </li>
                                <li class="p-2 hover:bg-gray-200">
                                    @include('icons.x', ['style'=>'h-5 w-5 float-right self-center text-gray-700'])
                                    Delete
                                </li>
                                <li class="p-2 hover:bg-gray-200">🦄 Other action</li>
                            </ul>


                        </td>
                    </tr>

                    @empty

                    <tr>
                        <td>No Users found!</td>
                    </tr>

                    @endforelse

                </tbody>
            </table>
        </div>
    </div>

    <div class="flex justify-between">
        <div class="pt-2">
            Showing {{ $users->firstItem() }} to {{ $users->lastItem() }} out of {{ $users->total() }}
        </div>
        <div class="">
            {{ $users->links('livewire.livewire-pagination') }}
        </div>
    </div>



</section>