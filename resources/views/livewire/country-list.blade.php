<section class="py-3">
    <div class="rounded border">
        <div class="flex flex-wrap w-full bg-gray-100 px-3 py-2 border-b">
            <div class="md:w-1/5">
                <span class="block text-sm uppercase text-gray-700 font-semibold">Name</span>
            </div>
        </div>

        <ul class="data-list">
            @forelse ($list as $item)
            <li>
                <div class="flex justify-between">
                    <div class="inline-flex items-center">
                        <img src="{{$item->flag}}" alt="{{$item->name}}" class="w-10 mr-2">
                        {{ $item->name }}
                    </div>
                    <div class="inline-flex items-center">

                        {{-- @if ($countryList == 'visitedCountries')
                        <a href="" title="mark as favorite"
                            class="text-gray-600 hover:text-white hover:bg-pink-700 p-2 rounded-full">
                            @includeif('icons.heart')
                        </a>
                        @else
                        <a href="" class="text-gray-600 hover:text-white hover:bg-indigo-700 p-2 rounded-full"
                            title="mark as visited">
                            @includeif('icons.visited')
                        </a>
                        @endif --}}

                        <form action="{{ route('destinations.destroy', $item->id )}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="status" value="visited">
                            <button type="submit" title="Delete from this list"
                                class="inline-flex text-gray-600 hover:text-white hover:bg-red-600 p-2 rounded-full focus:outline-none">
                                @includeif('icons.trash')
                            </button>
                        </form>
                    </div>
                </div>
            </li>
            @empty
            <li>
                This list is empty. Add countries to your list!
            </li>
            @endforelse
        </ul>
    </div>
    <div class="flex justify-center py-2">
        {{ $list->links('livewire.livewire-pagination') }}
    </div>
</section>