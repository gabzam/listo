@extends('layouts.app')

@section('title', 'HomePage -')

@section('description')
Default profile page when user logs in
@endsection

@section('content')

<h1 class="text-2xl font-bold pt-6">Dashboard</h1>

<div class="card-body">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    Hi {{ auth()->user()->firstname }} You are logged in!
    <br>
    <div class="w-full md:w-1/3 my-10">
        Welcome to yet another version of this app! <br><br>
        First of all, thank you for helping me out!

        If you have any comment, observation, feedback, issue, compliment, bug to report please write it here
        <br><br>
        <a href="https://trello.com/b/blqXhmgo/listo-public"
            class="bg-pink-700 hover:bg-pink-800 hover:text-gray-100 rounded text-white px-3 py-2"
            target="_blank">Trello
            public board</a>
    </div>

    <iframe src="https://giphy.com/embed/l41K6N4ITDN22au0U" width="480" height="385" frameBorder="0" class="giphy-embed"
        allowFullScreen></iframe>
    <p><a href="https://giphy.com/gifs/youngertv-tv-land-tvland-l41K6N4ITDN22au0U">via GIPHY</a></p>


    <h1 class="mt-10 mb-3">Choosing a colors and buttons</h1>
    <a href="#"
        class="py-2 px-3 mx-2 rounded bg-indigo-600 text-white hover:text-indigo-100 hover:bg-indigo-800">Button</a>
    <a href="#" class="py-2 px-3 mx-2 rounded bg-black text-white hover:text-white">Button</a>
    <a href="#" class="py-2 px-3 mx-2 rounded bg-gray-300 text-black">Button</a>
    <a href="#" class="py-2 px-3 mx-2 rounded bg-purple-600 text-white">Button</a>
    <a href="#" class="py-2 px-3 mx-2 rounded bg-pink-700 text-white hover:text-pink-900 hover:bg-pink-200">Button</a>

    <h1 class="mt-10 mb-3">Choosing shapes</h1>
    <a href="#" class="py-2 px-3 mx-2 bg-black text-white hover:text-white">Button</a>
    <a href="#" class="py-2 px-3 mx-2 rounded-full bg-purple-600 text-white">Button</a>
    <a href="#" class="py-2 px-3 mx-2 rounded bg-pink-700 text-white hover:text-pink-800 hover:bg-pink-300">Button</a>
</div>
@endsection

@section('scripts')

@endsection