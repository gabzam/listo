@extends('layouts.app')

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        Permission
    </h2>
</header>

<table>
    <tbody>
        <tr>
            <td class="text-right">
                <strong>Name:</strong>
            </td>
            <td class="pl-2">
                {{ $permission->name }}
            </td>
        </tr>
        <tr>
            <td class="text-right"><strong>Label:</strong></td>
            <td class="pl-2">
                {{ $permission->label }}
            </td>
        </tr>
        <tr>
            <td class="text-right"><strong>Slug:</strong></td>
            <td class="pl-2">
                {{ $permission->slug }}
            </td>
        </tr>
        <tr>
            <td class="text-right"><strong>Roles:</strong></td>
            <td class="pl-2">
                <ul>
                    {{-- @foreach ($permission->roles() as $item)
                    <li>{{ $item->name }}</li>
                    @endforeach --}}
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<div class="my-10">
    <a href="{{ route('permissions.edit', $permission->id)}}"
        class="bg-black text-white py-2 px-3 rounded hover:text-white hover:bg-pink-700 leading-none">Edit
        permisson</a>
    <form action="{{ route('permissions.destroy', $permission->id )}}" method="post" class="inline">
        @csrf
        @method('delete')
        <button type="submit"
            class="bg-red-700 text-white py-2 px-3 rounded hover:text-white hover:bg-pink-700 leading-none">Delete</button>
    </form>
</div>

@endsection