@extends('layouts.app')

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        Permissions
    </h2>
</header>


<section id="permissions-list" class="mb-16 border rounded">
    <div class="flex flex-wrap w-full bg-gray-100 px-3 py-2 border-b">
        <div class="md:w-1/5">
            <span class="block text-sm uppercase text-gray-700 font-semibold">Name</span>
        </div>
        <div class="hidden md:inline w-1/5">
            <span class="block text-sm uppercase text-gray-700 font-semibold">Slug</span>
        </div>
        <div class="hidden md:inline w-2/5">
            <span class="block text-sm uppercase text-gray-700 font-semibold">Label</span>
        </div>
        <div class="hidden md:inline md:w-1/5 text-right">
            <a href="{{route('permissions.create')}}"
                class="bg-black py-1 px-2 text-sm text-gray-100 hover:bg-pink-800 hover:text-white focus:outline-none rounded">
                @include('icons.add')<span class="ml-2">Add</span>
            </a>
        </div>
    </div>
    <ul class="data-list">
        @forelse ($permissions as $permission)
        <li>
            <a href="{{ route('permissions.show', $permission->id )}}" class="inline-flex w-full block">
                <div class="flex flex-wrap w-full">
                    <div class="w-full md:w-1/5">
                        <strong class="capitalize block">{{$permission->name}}</strong>
                        <span class="md:hidden block">
                            {{ $permission->slug }}
                        </span>
                        <span class="md:hidden">
                            {{ $permission->label }}
                        </span>
                    </div>
                    <div class="hidden md:inline w-full md:w-1/5">
                        {{ $permission->slug }}
                    </div>
                    <div class="hidden md:inline w-full md:w-2/5">
                        {{ $permission->label }}
                    </div>
                    <div class="hidden md:inline w-1/5">
                        @include('icons.next', ['style'=>'h-5 w-5 float-right self-center text-gray-700'])
                    </div>
                </div>
            </a>
        </li>
        @empty

        <li>No Permissons found!</li>

        @endforelse
    </ul>


</section>
@endsection