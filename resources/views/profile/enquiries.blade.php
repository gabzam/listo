@extends('layouts.app')

@section('content')

<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        My Enquiries
    </h2>
</header>

@include('enquiries.details.list', ['enquiries' => $user->enquiries()->paginate(10)] )


@endsection