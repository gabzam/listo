@extends('layouts.app')

@section('content')
<div class="my-20 flex justify-center">
    <div class="w-1/2">
        <iframe src="https://giphy.com/embed/5uuRWk7fiXeM0" width="100%" height="445" frameBorder="0"
            class="giphy-embed" allowFullScreen></iframe>
        <p class="text-center"><a href="https://giphy.com/gifs/raccoon-hank-green-michael-aranda-5uuRWk7fiXeM0">via
                GIPHY</a></p>
    </div>
</div>
@endsection