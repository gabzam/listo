@extends('layouts.app')

@section('content')
<div class="my-20 flex justify-center">
    <div class="w-1/2">
        <iframe src="https://giphy.com/embed/3o72FkiKGMGauydfyg" width="100%" height="363" frameBorder="0"
            class="giphy-embed" allowFullScreen></iframe>
        <p class="text-center"><a href="https://giphy.com/gifs/arielle-m-coming-soon-3o72FkiKGMGauydfyg">via GIPHY</a>
        </p>
    </div>
</div>

@endsection