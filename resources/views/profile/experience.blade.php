@extends('layouts.app')

@section('content')
<div class="my-20 flex justify-center">
    <div class="w-1/2">
        <iframe src="https://giphy.com/embed/ncU3bkZ5ghDlS" width="100%" height="299" frameBorder="0"
            class="giphy-embed" allowFullScreen></iframe>
        <p class="text-center"><a href="https://giphy.com/gifs/soon-ncU3bkZ5ghDlS">via GIPHY</a></p>
    </div>
</div>

@endsection