@extends('layouts.app')

@php
$title = 'New Category';
$name = '';
$description = '';
$icon = '';
$method = 'POST';
$button = 'Create category';
$btn_icon = 'icons.add';
@endphp

@isset($category)
@php
$title = 'Editing category: ' . $category->name ;
$name = $category->name;
$description = $category->description;
$icon = $category->icon;
$method = 'PUT';
$button = 'Save changes';
$btn_icon = 'icons.edit';
@endphp
@endisset

@section('content')
<header class="py-8">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        {{ $title }}
    </h2>
</header>

{{-- @include('shared.form_errors') --}}

<form class="kirby-form"
    action="{{ isset($category) ?  route('categories.update', $category->id) : route('categories.store') }}"
    method="POST">
    @csrf
    @method($method)
    {{-- Departure & Destination --}}
    <div class="mb-6">
        <label for="name">Name</label>
        <input id="name" type="text" name="name" placeholder="ex: Business" value="{{ $name }}"
            class="@error('name') field-error @enderror">
        @error('name')
        <p class=" italic text-red-600 text-sm">{{ $message }}</p>
        @enderror
    </div>

    <div class="mb-6">
        <label for="description">Description</label>
        <textarea name="description" id="description" cols="30" rows="5">{{ $description }}</textarea>
    </div>

    <div class="mb-6">
        <label for="icon">Icon</label>
        <input id="icon" type="text" name="icon" placeholder="ex: cross" value="{{ $icon }}">
    </div>

    <button type="submit">
        @include($btn_icon) {{ $button }}
    </button>
</form>
@endsection