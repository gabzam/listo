@extends('layouts.app')

@section('content')
<header class="py-16 text-center">
    <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate">
        {{ $category->name }}
    </h2>
    @if($category->icon)
    @php
    $icon = 'icons.' . $category->icon;
    @endphp
    <div class="text-center my-10">
        @include($icon, ['style' => 'h-10 w-10 block mx-auto'])
    </div>
    @endif
</header>
<div class="mb-10 w-1/2 mx-auto text-center">
    {{ $category->description }}
</div>

<div class="my-20 flex justify-center">
    <a href="{{ route('categories.edit', $category->id) }}" class="kirby-btn-edit hover:text-white">
        @include('icons.edit')
        Edit
    </a>
    <form action="{{ route('categories.destroy', $category->id )}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" class="kirby-btn-delete">
            @include('icons.trash')
            Delete
        </button>
    </form>
</div>

@endsection