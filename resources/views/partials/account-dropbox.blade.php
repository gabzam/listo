{{-- <button
    class="p-1 border-2 border-transparent text-gray-400 rounded-full hover:text-white focus:outline-none focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out">
    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
            d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
    </svg>
</button> --}}
<div @click.away="open = false" class="ml-3 relative" x-data="{ open: false }">
    <div>
        <button @click="open = !open"
            class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-white transition duration-150 ease-in-out">
            <img class="h-8 w-8 rounded-full" src="{{ asset(auth()->user()->avatar) }}"
                alt="{{ auth()->user()->username }}" />
        </button>
    </div>
    <div x-show="open" x-transition:enter="transition ease-out duration-100"
        x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100"
        x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100"
        x-transition:leave-end="transform opacity-0 scale-95"
        class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg">
        <ul class="absolute text-gray-700 pt-1 right-0 origin-top-right text-left w-64 bg-white rounded-lg shadow-lg">
            <li class="border-gray-200 py-1">
                <div class="flex items-center px-6 py-4">
                    <img class="h-10 w-10 rounded-full flex-no-shrink" src="{{ asset(auth()->user()->avatar) }}"
                        alt="{{ auth()->user()->username }}" />

                    <div class="ml-4">
                        <p class="font-semibold text-gray-900 leading-none">
                            {{ Auth::user()->firstname }}
                        </p>
                        <p>
                            <a href="{{ route('profile.show', Auth::user()->id) }}"
                                class="text-sm text-gray-600 leading-none hover:underline">
                                View Profile
                            </a>
                        </p>
                    </div>
                </div>
            </li>
            <li>
                <a class="hover:bg-gray-300 px-6 py-3 leading-tight block whitespace-no-wrap"
                    href="{{ route('home', auth()->user()->id )}}">
                    Dashboard
                    {{-- Settings and Privacy --}}
                </a>
            </li>
            {{-- <li>
                <a class="rounded hover:bg-gray-300 px-6 py-3 leading-tight block whitespace-no-wrap" href="#">
                    Language
                </a>
            </li>
            <li>
                <a class="rounded hover:bg-gray-300 px-6 py-3 leading-tight block whitespace-no-wrap" href="#">
                    Advertise
                </a>
            </li>
            <li>
                <a class="rounded hover:bg-gray-300 px-6 py-3 leading-tight block whitespace-no-wrap" href="#">
                    Analytics
                </a>
            </li> --}}
            <li class="border-t-2 border-gray-200 py-1">
                <a class="hover:bg-gray-300 px-6 py-3 leading-tight block whitespace-no-wrap"
                    href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </li>
        </ul>
    </div>
</div>