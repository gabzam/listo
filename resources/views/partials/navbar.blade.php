<nav x-data="{ open: false }" class="bg-white fixed w-full top-0 border-b z-50">
    <div class="px-2 sm:px-6">
        <div>
            <div class="relative flex items-center flex-wrap h-12">
                <div class="absolute inset-y-0 left-0 flex items-center sm:hidden">
                    <button @click="open = !open"
                        class="inline-flex items-center justify-center p-2 rounded-md text-gray-800 hover:text-pink-800 focus:outline-none transition duration-150 ease-in-out">
                        <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                            <path :class="{'hidden': open, 'inline-flex': !open }" class="inline-flex"
                                stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M4 6h16M4 12h16M4 18h16" />
                            <path :class="{'hidden': !open, 'inline-flex': open }" class="hidden" stroke-linecap="round"
                                stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </button>
                </div>
                <div class="flex-1 flex items-center justify-center sm:items-stretch sm:justify-center">
                    <a class="flex-shrink-0 text-gray-800 hover:text-pink-800 font-semibold text-xl tracking-tight align-middle"
                        href="{{ route('welcome') }}">
                        @include('icons.logo', ['style'=>'h-8 w-8 inline'])
                        <span class="hidden sm:inline">{{ config('app.name', 'Laravel') }}</span>
                    </a>
                </div>
                <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:pr-0">
                    <!-- Authentication Links -->
                    @guest
                    <a class="p-1 text-gray-800 hover:text-pink-800 focus:outline-none focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out"
                        href="{{ route('login') }}">{{ __('Login') }}</a>
                    @if (Route::has('register'))
                    <a class="p-1 text-gray-800 hover:text-pink-800 focus:outline-none focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out"
                        href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                    @else
                    @include('partials.account-dropbox')
                    @endguest
                </div>
            </div>
        </div>
    </div>
    <div :class="{'block': open, 'hidden': !open}" class="hidden sm:s">
        <div class="px-2 pt-2 pb-3">
            @include('partials.navigation', ['responsive' => 'text-base block'])
        </div>
    </div>
</nav>