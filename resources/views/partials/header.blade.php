<header class="py-10">
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="lg:flex lg:items-center lg:justify-between">
            <div class="flex-1 min-w-0">
                <h2 class="text-2xl font-bold leading-7 text-white sm:text-3xl sm:leading-9 sm:truncate">
                    {{ $title }}
                </h2>
                <div class="mt-1 flex flex-col sm:mt-0 sm:flex-row sm:flex-wrap">
                    <div class="mt-2 flex items-center text-sm leading-5 text-gray-300 sm:mr-6">
                        @include('icons.briefcase')
                        Full-time
                    </div>
                    <div class="mt-2 flex items-center text-sm leading-5 text-gray-300 sm:mr-6">
                        @include('icons.pinpoint')
                        Remote
                    </div>
                    <div class="mt-2 flex items-center text-sm leading-5 text-gray-300 sm:mr-6">
                        @include('icons.dollar')
                        $120k – $140k
                    </div>
                    <div class="mt-2 flex items-center text-sm leading-5 text-gray-300">
                        @include('icons.calendar')
                        Closing on January 9, 2020
                    </div>
                </div>
            </div>

            <div class="mt-5 flex lg:mt-0 lg:ml-4">
                <span class="sm:ml-3 shadow-sm rounded-md">
                    <button type="button"
                        class="inline-flex items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-500 hover:bg-indigo-400 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-600 transition duration-150 ease-in-out">
                        @include('icons.check')
                        Publish
                    </button>
                </span>

            </div>
        </div>
    </div>
</header>