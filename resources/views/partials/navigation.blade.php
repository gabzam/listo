@php
$selected = "text-black font-bold";
$others = "text-gray-800 hover:text-pink-800";
$all = "px-3 py-2 focus:outline-none focus:text-pink-800 transition duration-150 ease-in-out hover:bg-gray-200
rounded ";
@endphp


@auth
{{-- <a class="{{ Request::is('home') ? $selected : $others }} {{ $all . $responsive }}" href="{{ route('home') }}">
Dashboard
</a> --}}


<a class="{{ Request::is('home') ? $selected : $others }} {{ $all . $responsive }}" href="{{ route('home') }}">
    Dashboard
</a>

<a class="{{ Request::is('destinations') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('destinations.index') }}">
    My destinations
</a>

<a class="{{ Request::is('profile') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('profile.enquiries', auth()->user()->id) }}">
    My enquiries
</a>

<a class="{{ Request::is('challenges') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('challenges.index') }}">
    Challenges
</a>

<br>
<h5 class="uppercase text-sm block text-gray-700 font-semibold pl-3">Bucketlist</h5>
<hr class="mb-3">

<a class="{{ Request::is('profile.toVisit') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('profile.visit', auth()->user()->id) }}">
    Places to visit
</a>
<a class="{{ Request::is('profile.toLearn') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('profile.learn', auth()->user()->id) }}">
    Things to learn
</a>
<a class="{{ Request::is('profile') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('profile.experience', auth()->user()->id) }}">
    Experiences
</a>

{{-- <a class="{{$others}} {{ $all . $responsive }}" href="#">
Bucketlist
</a>

<a class="{{$others}} {{ $all . $responsive }}" href="#">
    Challenges
</a> --}}


{{-- <a class="{{ Request::is('enquiries') ? $selected : $others }} {{ $all . $responsive }}"
href="{{ route('enquiries.index') }}">
Enquiries
</a>
<a class="{{ Request::is('destination') ? $selected : $others }} {{ $all . $responsive }}" href="#">
    Destinations
</a>--}}

<div class="my-12 hidden md:block"></div>

{{-- <a class="{{$others}} {{ $all . $responsive }}" href="#">
Places
</a> --}}

<h5 class="uppercase text-sm block text-gray-700 font-semibold pl-3">Admin</h5>
<hr class="mb-3">
<a class="{{ Request::is('enquiries') ? $selected : $others}} {{ $all . $responsive }}"
    href="{{ route('enquiries.index')}}">
    Enquiries
</a>




<a class="{{ Request::is('users') ? $selected : $others }} {{ $all . $responsive }}" href="{{ route('users.index') }}">
    Users
</a>

<a class="{{ Request::is('companies') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('companies.index') }}">
    Companies
</a>

<a class="{{ Request::is('countries') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('countries.index') }}">
    Countries
</a>

<a class="{{ Request::is('categories') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('categories.index') }}">
    Categories
</a>

<a class="{{ Request::is('roles') ? $selected : $others }} {{ $all . $responsive }}" href="{{ route('roles.index') }}">
    Roles
</a>

<a class="{{ Request::is('permissions') ? $selected : $others }} {{ $all . $responsive }}"
    href="{{ route('permissions.index') }}">
    Permissions
</a>


@endauth