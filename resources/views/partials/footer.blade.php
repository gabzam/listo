<footer class="py-5 mt-5">
    <div class="text-center">
        <a class="inline-flex text-gray-800 hover:text-pink-800 mb-5 font-semibold text-2xl tracking-tight align-middle"
            href="{{ route('welcome') }}">
            @include('icons.logo',['style' => 'h-10 w-10 block mx-auto']) {{ config('app.name', 'Laravel') }}
        </a>
        <br>
        <a href="" class="text-gray-800 mr-2 my-5">Terms & Conditions</a>
        <a href="" class="text-gray-800 ml-2 my-5">General Conditions</a>
        <div class="mt-5">
            Listo &copy; 2020
        </div>
    </div>
</footer>