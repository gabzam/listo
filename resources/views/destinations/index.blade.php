@extends('layouts.app')

@section('head')
<link rel="stylesheet" type="text/css" href="//github.com/downloads/lafeber/world-flags-sprite/flags16.css" />
@endsection

@section('content')
<header class="pt-8 pb-4 flex flex-wrap justify-between items-center">
    <div class="w-full md:w-1/3 mx-3 mb-2">
        <h2 class="text-2xl font-bold leading-7 sm:text-3xl sm:leading-9 sm:truncate text-gray-800">
            My destinations
        </h2>
    </div>
    <div class="w-full md:w-1/3 text-right mx-3">
        <a href="{{ route('enquiries.create') }}"
            class="bg-red-600 text-white rounded py-2 px-3 hover:bg-indigo-700 hover:text-white w-full md:w-auto text-center">
            Request a travel offer
        </a>
    </div>
</header>

<div class="flex flex-wrap -px-3">
    <div class="w-full md:w-3/4 mb-5 md:mb-0 px-3">
        <div id="regions_div" class="w-full h-full rounded-lg overflow-hidden"></div>
    </div>
    <div class="w-full md:w-1/4 px-3 flex flex-col justify-between -my-1">

        <div class="border rounded-lg text-center py-3 my-1 hover:shadow bg-white">
            <span class="text-6xl block font-bold text-gray-700">{{ $countryList->count() }}</span>
            <span class="uppercase text-gray-600">Total countries</span>
        </div>

        <div class="border rounded-lg text-center py-3 my-1 hover:shadow bg-white">
            <span
                class="text-6xl block font-bold text-green-500 mb-0 pb-0">{{ $user->visitedCountries->count() }}</span>
            <span class="uppercase text-gray-600">visited countries</span>
        </div>

        <div class="border rounded-lg text-center py-3 my-1 hover:shadow bg-white">
            <span class="text-6xl block text-red-600 font-bold">{{ $user->bucketListCountries->count() }} </span>
            <span class="uppercase text-gray-600">Countries to visit</span>
        </div>
        {{-- <div id="gauge" class="w-full"></div> --}}
    </div>
</div>

<br>
<br>

<section class="flex flex-wrap -px-2">
    <div class="w-full md:w-1/2 px-2">
        <div x-data="{ open: false }">
            <div class="flex justify-between items-end">
                <h2 class="text-lg font-bold text-gray-800">Visited Countries</h2>
                <button
                    class="inline-flex items-center mt-4 bg-gray-300 py-1 px-2 rounded text-gray-800 hover:bg-pink-700 hover:text-white focus:outline-none"
                    @click="open = true" onclick="focusMethod()" x-show="!open">
                    <input type="hidden" name="status" value="value">
                    @include('icons.add') <span class="pl-2">Add</span>
                </button>
            </div>
            <div x-show="open" @click.away="open = false" class="mt-4">
                @include('shared.country_input', ['status'=> 'visited', 'label'=>'Add country and press enter'])
            </div>
        </div>
        @livewire('country-list', ['list'=> 'visitedCountries'])
    </div>

    <div class="w-full md:w-1/2 px-2">
        <div x-data="{ open: false }">
            <div class="flex justify-between items-end">
                <h2 class="text-lg font-bold text-gray-800">Bucketlist</h2>
                <button
                    class="inline-flex items-center mt-4 bg-gray-300 py-1 px-2 rounded-full text-gray-800 hover:bg-indigo-700 hover:text-white focus:outline-none"
                    @click="open = true" onclick="focusMethod()" x-show="!open">
                    @include('icons.add') <span class="pl-2">Add</span>
                </button>
            </div>

            <div x-show="open" @click.away="open = false" class="mt-4">
                @include('shared.country_input', ['status'=> 'bucketlist', 'label'=>'Add country this'])
            </div>
        </div>
        @livewire('country-list', ['list'=> 'bucketlistCountries'])
    </div>
</section>

@endsection


@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/places.js@1.18.1"></script>
<script>
    console.log('name:' + gabo);
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    let visited = <?= json_encode($user->visitedCountries->toArray()); ?>;
    let bucketlist = <?= json_encode($user->bucketListCountries->toArray()); ?>;
    
    google.charts.load('current', {
        'packages': ['geochart'],
        // Note: you will need to get a mapsApiKey for your project.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
    });

    google.charts.setOnLoadCallback(drawRegionsMap);

    function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable([]);
        data.addColumn('string', 'Country');
        data.addColumn('number', 'Want to go');
        visited.forEach(element => { 
            data.addRow([element.alpha_2_Code,100]); 
        });
        
        bucketlist.forEach(element => { 
            data.addRow([element.alpha_2_Code, 0]); 
        });
        

        var options = {
            //region: '002', // Africa
            colorAxis: {colors: ['#84dcc6', '#ff595e']},
            defaultColor: '#ff595e',
            backgroundColor: '#a9def9',
            legend: 'none'
        };

    var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

    chart.draw(data, options);
    }

    
    google.charts.load('current', {'packages':['gauge']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Visited', visited.length],
          ['Bucketlist', bucketlist.length],          
        ]);

        var options = {
          width: 350, height: 500,
          redFrom: 200, redTo: 250,
          yellowFrom: 180, yellowTo: 200,
          minorTicks: 5,
          max: 250,
        };

        var chart = new google.visualization.Gauge(document.getElementById('gauge'));

        chart.draw(data, options);
    }
</script>
@endsection