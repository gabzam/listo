<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('icons', 'IconController@index');

Auth::routes(['verify' => true]);

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('categories','CategoryController');
    Route::resource('countries','CountryController');
    Route::resource('users','UserController');
    Route::resource('agencies','AgencyController');
    Route::resource('companies','CompanyController');
    Route::resource('enquiries','EnquiryController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('challenges','ChallengeController');
    
    Route::get('profile/{user}/enquiries','ProfileController@enquiries')->name('profile.enquiries');
    Route::get('profile/{user}','ProfileController@show')->name('profile.show');
    Route::get('profile/{user}/visit','ProfileController@visit')->name('profile.visit');
    Route::get('profile/{user}/learn','ProfileController@learn')->name('profile.learn');
    Route::get('profile/{user}/experience','ProfileController@experience')->name('profile.experience');

    
    Route::get('destinations','DestinationController@index')->name('destinations.index');
    
    Route::post('destinations','DestinationController@store')->name('destinations.store');
    Route::delete('destinations/{country}','DestinationController@destroy')->name('destinations.destroy');
    //Route::post('destinations','DestinationController@bucketlist')->name('destinations.bucketlist');
    
    Route::get('visits', function(){
        return 'hola';
    })->name('visits');
    
    Route::get('trashed-agencies','CompanyController@trashed')->name('trashed_agencies.trashed');
    Route::get('trashed-agencies/{agency}','CompanyController@show_trashed')->name('trashed_agencies.show_trashed');
    Route::put('restore-agency/{agency}','CompanyController@restore')->name('restored_agency');    
});
