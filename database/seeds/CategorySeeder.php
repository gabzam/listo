<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name'          => 'Romance',
            'description'   => 'Activities associated with love made by couple',
            'icon'          => 'heart',
        ]);

        Category::create([
            'name'          => 'Shopping',
            'description'   => 'Travel focused on the activity of buying goods from shops, mostly refering to clothings, and jewelry',
            'icon'          => 'shopping',
        ]);

        Category::create([
            'name'          => 'Cultural',
            'description'   => 'Tourism focused on learning customs, arts, and social behaviour of another society',
            'icon'          => 'cultural',
        ]);

        Category::create([
            'name'          => 'Sport',
            'description'   => 'Travel where people practice, or compete in any athletic activity for accomplishment, fun or health',
            'icon'          => 'basketball',
        ]);
        
        Category::create([
            'name'          => 'Leisure',
            'description'   => 'Any activities done during free time for enjoyment and entertainment',
            'icon'          => 'leisure',
        ]);
        
        Category::create([
            'name'          => 'Sightseeing',
            'description'   => 'Visiting places of interest in a particular location',
            'icon'          => 'sightseeing',
        ]);
        
        Category::create([
            'name'          => 'Nature',
            'description'   => 'Activities researching a connection with mother nature, including plants, animals, the landscape, and other features and products of the earth, as opposed to humans or human creations',
            'icon'          => 'nature',
        ]);
        
        Category::create([
            'name'          => 'Medical',
            'description'   => 'A travel with the purpose of an examination to assess a person\'s state of physical health or fitness',
            'icon'          => 'medical-heart',
        ]);
        
        Category::create([
            'name'          => 'Business',
            'description'   => 'Tourism directed towards work and professional activities',
            'icon'          => 'business',
        ]);
        
        Category::create([
            'name'          => 'Discovery',
            'description'   => 'Tourism directed towards exploring new habits, new places, new foods, new things',
            'icon'          => 'compass',
        ]);
        
        Category::create([
            'name'          => 'Well-being',
            'description'   => 'Activities that take a person into a state of being comfortable, relaxed, healthy, and happy',
            'icon'          => 'meditation',
        ]);
        
        Category::create([
            'name'          => 'Beach',
            'description'   => 'A travel in research of sun, water and sandy shores, especially by the sea',
            'icon'          => 'palm',
        ]);
        
        Category::create([
            'name'          => 'Wedding',
            'description'   => 'After marriage ceremony, a honeymoon travel involving romantic activities for couples',
            'icon'          => 'wedding',
        ]);
        
        Category::create([
            'name'          => 'Safari',
            'description'   => 'An expedition to observe or hunt animals in their natural habitat, especially in East Africa.',
            'icon'          => 'elephant',
        ]);
        
        Category::create([
            'name'          => 'Eco-Tourism',
            'description'   => 'Tourism directed towards exotic, often threatened, natural environments, intended to support conservation efforts and observe wildlife',
            'icon'          => 'eco-tourism',
        ]);

        Category::create([
            'name'          => 'Religion',
            'description'   => 'Tourism where people can worship their God by visiting temples and/or historic sites',
            'icon'          => 'religion',
        ]);

        Category::create([
            'name'          => 'Academic',
            'description'   => 'Relating to education and scholarship',
            'icon'          => 'academic',
        ]);

        Category::create([
            'name'          => 'Gastronomy',
            'description'   => 'The practice of discovering, choosing, cooking, and eating good food',
            'icon'          => 'gastronomy',
        ]);
    }
}
