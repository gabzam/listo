<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::where('email','gab.zambrano@gmail.com')->first();

        if (!$user) {
            User::create([
                'firstname'     => 'Gabriel',
                'lastname'      => 'Zambrano',
                'email'         => 'gab.zambrano@gmail.com',
                'password'      => Hash::make('password'),
                'username'      => 'gabzon',
                'profession'    => 'entrepreneur',                                
                'picture'       => '',
                'gender'        => 'male',
                'mobile'        => '+41 76 571 4931',
                'phone'         => '+385 99 648 3693',
                'whatsapp'      => 1,
                'viber'         => 1,
                'wechat'        => 1,
                'telegram'      => 1,
                'call'          => 1,
                'sms'           => 1,
                'street'        => 'Karašička ulica',
                'street_number' => '29',
                'address_info'  => '',
                'postal_code'   => '10000',
                'location'      => 'Zagreb',
                'state'         => 'Zagreb',
                'country'       => 'Croatia',
                'facebook'      => 'https://www.facebook.com/',
                'instagram'     => 'https://www.instagram.com/uespiiiiii/',
                'linkedin'      => 'https://hr.linkedin.com/',
                'twitter'       => '@gab_zam',
                'skype'         => 'gabrielvinci',
                'snapchat'      => '@elgato',
                'tiktok'        => '@gabzon',
                'youtube'       => 'https://www.youtube.com/',
                'email_verified_at' => now(),      
            ]);
        }
    
        factory(App\User::class, 25)->create();
    }
}




