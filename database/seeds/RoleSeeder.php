<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate([
            'name'  => 'admin',
            'slug'  => 'admin',
            'label' => 'Administrator of the website, full access, all rigths granted',
        ]);

        Role::firstOrCreate([
            'name'  => 'user',
            'slug'  => 'user',
            'label' => 'Default user',
        ]);

        Role::firstOrCreate([
            'name'  => 'agent',
            'slug'  => 'agent',
            'label' => 'A person who works in a travel agency',
        ]);

        Role::firstOrCreate([
            'name'  => 'Assistant',
            'slug'  => 'assistant',
            'label' => 'A person helps me with this project',
        ]);
    }
}
