<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {        
        $this->call([
            PermissionSeeder::class,
            RoleSeeder::class,            
            CategorySeeder::class,
            CountryTableSeeder::class,
            CompanySeeder::class,
            UserSeeder::class,
            ]
        );        
    }
}
