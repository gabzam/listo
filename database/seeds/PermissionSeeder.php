<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name'  => 'View Requests',
            'slug'  => 'view_requests',
            'label' => 'A person who can view the list of all requests in the database',
        ]);

        Permission::create([
            'name'  => 'Edit Requests',
            'slug'  => 'edit_requests',
            'label' => 'A person who can edit any request in the database',
        ]);

        Permission::create([
            'name'  => 'Delete Requests',
            'slug'  => 'delete_requests',
            'label' => 'A person who can delete any request from the database',
        ]);

        Permission::create([
            'name'  => 'View Countries',
            'slug'  => 'view_countries',
            'label' => 'A person who has access to the full list of countries in the database',
        ]);

        Permission::create([
            'name'  => 'Create Country',
            'slug'  => 'create_country',
            'label' => 'A person who can create a country in the country list',
        ]);
    }
}
