<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name'          => $faker->company,
        'description'   => $faker->paragraph(rand(2,4)),
        'email'         => $faker->unique()->safeEmail,
        'phone'         => $faker->unique()->e164PhoneNumber,
        'website'       => $faker->url,
        
        'employees'     => rand(0,8),
        'has_branches'  => $faker->optional()->boolean,
        'has_insurance' => $faker->optional()->boolean,
        'has_newsletter'=> $faker->optional()->boolean,
        'founded'       => $faker->date('Y-m-d'),
        'defunct'       => $faker->optional()->date('Y-m-d'),

        'street'        => $faker->streetAddress,
        'street_number' => $faker->buildingNumber,
        'address_info'  => $faker->optional()->secondaryAddress,
        'postal_code'   => $faker->postcode,
        'location'      => $faker->city,
        'state'         => $faker->state,
        'country'       => $faker->country,
        'lat'           => $faker->latitude,
        'lng'           => $faker->longitude,        
        
        'facebook'      => $faker->optional()->url,
        'twitter'       => $faker->optional()->url,
        'instagram'     => $faker->optional()->url,
        'linkedin'      => $faker->optional()->url,
        'snapchat'      => $faker->optional()->url,
        'blog'          => $faker->optional()->url,
        'pinterest'     => $faker->optional()->url,
        'youtube'       => $faker->optional()->url,
        'skype'         => $faker->optional()->userName,
    ];
});
