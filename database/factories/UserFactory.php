<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {    
    $gender = $faker->randomElement(['male', 'female']);
    $mobile = $faker->e164PhoneNumber;
    $phone = $faker->optional()->e164PhoneNumber;
    
    $firstname = $faker->firstNameMale;
    $picture = 'https://randomuser.me/api/portraits/men/' . rand(1,99) . '.jpg';

    if ($gender === 'female') {
        $firstname = $faker->firstNameFemale;
        $picture = 'https://randomuser.me/api/portraits/women/' . rand(1,99) . '.jpg';
    }

    return [
        'firstname'         => $firstname,
        'lastname'          => $faker->lastName,
        'email'             => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token'    => Str::random(10),
        
        'username'      => $faker->unique()->userName,
        'profession'    => $faker->optional()->jobTitle,
        'birthday'      => $faker->optional()->date('Y-m-d'),
        'picture'       => $picture,
        'gender'        => $gender,
        'biography'     => $faker->paragraph,
        
        'mobile'        => $mobile,
        'phone'         => $phone,
        'call'          => $faker->boolean,
        'sms'           => $faker->boolean,
        'whatsapp'      => $faker->boolean,
        'viber'         => $faker->boolean,
        'wechat'        => $faker->boolean,
        'telegram'      => $faker->boolean,
        
        'street'        => $faker->streetAddress,
        'street_number' => $faker->buildingNumber,
        'address_info'  => $faker->optional()->secondaryAddress,
        'postal_code'   => $faker->postcode,
        'location'      => $faker->city,
        'state'         => $faker->state,
        'country'       => $faker->country,
        'lat'           => $faker->latitude,
        'lng'           => $faker->longitude,

        'facebook'      => $faker->optional()->url,
        'instagram'     => $faker->optional()->url,
        'linkedin'      => $faker->optional()->url,
        'twitter'       => $faker->optional()->url,
        'pinterest'     => $faker->optional()->url,
        'skype'         => $faker->optional()->userName,
        'snapchat'      => $faker->optional()->userName,
        'tiktok'        => $faker->optional()->userName,
        'youtube'       => $faker->optional()->url,

        'passport1'         => $faker->optional()->country,
        'passport1_expiry'  => $faker->optional()->date('Y-m-d'),
        'passport2'         => $faker->optional()->country,
        'passport2_expiry'  => $faker->optional()->date('Y-m-d'),
        'passport3'         => $faker->optional()->country,
        'passport3_expiry'  => $faker->optional()->date('Y-m-d'),
        'passport4'         => $faker->optional()->country,
        'passport4_expiry'  => $faker->optional()->date('Y-m-d'),
    ];

});