# Additing Tailwind to Laravel
- https://www.5balloons.info/getting-started-with-tailwindcss-on-laravel/
- https://www.itechempires.com/2019/07/how-to-use-tailwindcss-with-laravel-framework/amp/
- https://polished-sunset-0ex44e5cb9xt.tailwindcss.com/previews/mZOl4nEdBiYkg9Nw
- https://www.tailwindtoolbox.com/
- https://tailwindcomponents.com/
- https://tailwindui.com/page-examples/detail-view-01

# Design icons & illustrations
* https://linearicons.com/free
* https://iconmonstr.com/
* https://feathericons.com/
* https://jakearchibald.github.io/svgomg/
* https://undraw.co/illustrations

# Verify SMS
- https://www.twilio.com/blog/verify-phone-numbers-php-laravel-application-twilio-verify
- nexmo

# Graphs
- https://cinwell.com/vue-trend/

# Ecommerce
- https://www.youtube.com/watch?v=l8RyUFOB4Q0&list=PLrUNEklLHiKVUXsy41qwXn7nyeWkWiNha&index=10
- https://www.larashout.com/laravel-ecommerce-application-development-introduction

# Navigation 
- https://laracasts.com/series/laravel-6-from-scratch/episodes/16  (active menu link)

# youtube
* https://www.youtube.com/watch?v=saZRMJLYmmY&list=PLLUtELdNs2ZaHaFmydqjcQ-YyeQ19Cd6u


# tools
* https://www.vuetoolbox.com/

# Laravel 
* https://www.laravelbestpractices.com/#views
* https://gist.github.com/simonhamp/549e8821946e2c40a617c85d2cf5af5e (collections paginator)

https://github.com/Labs64/laravel-boilerplate#how-tos--modules-configuration


# PHP
* coalescing operator (https://www.tutorialspoint.com/php7/php7_coalescing_operator.htm)

https://speckyboy.com/free-bootstrap-admin-themes/
https://www.creative-tim.com/learning-lab/tailwind-starter-kit#/presentation
https://awesomeopensource.com/project/aniftyco/awesome-tailwindcss

# Best Practises
* https://github.com/alexeymezenin/laravel-best-practices
* https://www.laravelbestpractices.com/
* https://www.cloudways.com/blog/laravel-best-practices/
* https://www.codechief.org/article/laravel-6-form-validation-with-error-messages
* https://programming.vip/docs/laravel-best-practices.html

# Packages
* https://github.com/chiraggude/awesome-laravel
* https://github.com/JosephSilber/bouncer
* https://github.com/spatie/laravel-permission
* https://www.cloudways.com/blog/best-laravel-packages/
* https://laravel-news.com/category/laravel-packages
* https://laracasts.com/discuss/channels/laravel/authentication-for-3-types-of-users?page=1
* https://quickadminpanel.com/blog/two-free-laravel-6-roles-permissions-starter-projects/
https://www.innofied.com/top-10-laravel-best-practices/

# Designs
* https://dribbble.com/shots/5400896-Dashboard-Bank-Asia-internet-banking-Redesign/attachments
* https://dribbble.com/shots/5600797-Task-summary-Dashboard-Admin/attachments
* https://dribbble.com/shots/5652394-HR-Dashboard/attachments
* https://dribbble.com/shots/6901297-Customer-Care-Dashboard-Concept/attachments
* https://dribbble.com/shots/1315388-Dashboard-Web-App-Product-UI-Design-Job-Summary/attachments/184703
* https://medium.muz.li/dashboards-inspiration-2018-77b3ab185483
* https://1stwebdesigner.com/admin-dashboard-layouts/
- https://codepen.io/aaroniker/pen/XxPNqY
- https://stackoverflow.com/questions/45557798/put-arrow-or-triangle-on-end-of-line
- https://lokesh-coder.github.io/pretty-checkbox/

# Password Generation
- https://laracasts.com/discuss/channels/laravel/generate-password

# Multiple query
- https://stackoverflow.com/questions/46436176/laravel-pass-a-variable-with-query-builder-function
- https://laravel.io/forum/11-22-2014-querying-a-many-to-many-relationship
- https://www.itsolutionstuff.com/post/how-to-use-wherehas-with-orwherehas-in-laravelexample.html


# kreativmedia
- https://www.kreativmedia.ch/fr/commandes-ssh-chrooted
- https://laravel-news.com/laravel-5-4-key-too-long-error

- https://github.com/calebporzio/laracasts-livewire-datatable/blob/master/app/Contact.php
